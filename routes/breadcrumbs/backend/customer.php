<?php
// Home > Customer
Breadcrumbs::for('customers.index', function ($trail) {
    $trail->parent('admin.home');
    $trail->push('Customers', route('customers.index'));
});
// Home > Customers > Edit
Breadcrumbs::for('customers.edit', function ($trail, $id) {
    $trail->parent('customers.index');
    $trail->push('Edit Customer', route('customers.edit', $id));
});
// Home > Customers > Show
Breadcrumbs::for('customers.show', function ($trail, $id) {
    $trail->parent('customers.index');
    $trail->push('Show Customer', route('customers.show', $id));
});
