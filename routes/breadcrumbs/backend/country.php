<?php
// Home > Countries
Breadcrumbs::for('countries.index', function ($trail) {
    $trail->parent('admin.home');
    $trail->push('Countries', route('countries.index'));
});
// Home > create_Country
Breadcrumbs::for('countries.create', function ($trail) {
    $trail->parent('admin.home');
    $trail->push('Create Country', route('countries.create'));
});
// Home > Countries > Edit
Breadcrumbs::for('countries.edit', function ($trail, $id) {
    $trail->parent('countries.index');
    $trail->push('Edit Country', route('countries.edit', $id));
});
// Home > Countries > Show
Breadcrumbs::for('countries.show', function ($trail, $id) {
    $trail->parent('countries.index');
    $trail->push('Show Country', route('countries.show', $id));
});
