<?php
// Home > Departments
Breadcrumbs::for('departments.index', function ($trail) {
    $trail->parent('admin.home');
    $trail->push('Departments', route('departments.index'));
});
// Home > create_Department
Breadcrumbs::for('departments.create', function ($trail) {
    $trail->parent('admin.home');
    $trail->push('Create Department', route('departments.create'));
});
// Home > Departments > Edit
Breadcrumbs::for('departments.edit', function ($trail, $id) {
    $trail->parent('departments.index');
    $trail->push('Edit Department', route('departments.edit', $id));
});
// Home > Departments > Show
Breadcrumbs::for('departments.show', function ($trail, $id) {
    $trail->parent('departments.index');
    $trail->push('Show Department', route('departments.show', $id));
});
