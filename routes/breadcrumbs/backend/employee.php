<?php
// Home > Employee
Breadcrumbs::for('employees.index', function ($trail) {
    $trail->parent('admin.home');
    $trail->push('Employees', route('employees.index'));
});
// Home > create_Employee
Breadcrumbs::for('employees.create', function ($trail) {
    $trail->parent('admin.home');
    $trail->push('Create Employee', route('employees.create'));
});
// Home > Employees > Edit
Breadcrumbs::for('employees.edit', function ($trail, $id) {
    $trail->parent('employees.index');
    $trail->push('Edit Employee', route('employees.edit', $id));
});
// Home > Employees > Show
Breadcrumbs::for('employees.show', function ($trail, $id) {
    $trail->parent('employees.index');
    $trail->push('Show Employee', route('employees.show', $id));
});
