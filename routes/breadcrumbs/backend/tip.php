<?php
// Home > Tips
Breadcrumbs::for('tips.index', function ($trail) {
    $trail->parent('admin.home');
    $trail->push('Tips', route('tips.index'));
});
// Home > create_Tip
Breadcrumbs::for('tips.create', function ($trail) {
    $trail->parent('admin.home');
    $trail->push('Create Tip', route('tips.create'));
});
// Home > Tips > Edit
Breadcrumbs::for('tips.edit', function ($trail, $id) {
    $trail->parent('tips.index');
    $trail->push('Edit Tip', route('tips.edit', $id));
});
// Home > Tips > Show
Breadcrumbs::for('tips.show', function ($trail, $id) {
    $trail->parent('tips.index');
    $trail->push('Show Tip', route('tips.show', $id));
});
