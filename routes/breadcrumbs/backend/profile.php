<?php
// Home > Profile
Breadcrumbs::for('profile.index', function ($trail) {
    $trail->parent('admin.home');
    $trail->push('Profile', route('profile.index'));
});
// Home > create_Profile
Breadcrumbs::for('profile.create', function ($trail) {
    $trail->parent('admin.home');
    $trail->push('Create Profile', route('profile.create'));
});
// Home > Profile > Edit
Breadcrumbs::for('profile.edit', function ($trail, $id) {
    $trail->parent('profile.index');
    $trail->push('Edit Profile', route('profile.edit', $id));
});
// Home > Profile > Show
Breadcrumbs::for('profile.show', function ($trail, $id) {
    $trail->parent('profile.index');
    $trail->push('Show Profile', route('profile.show', $id));
});