<?php
//Home
Breadcrumbs::for('admin.home', function ($trail) {
    $trail->push(__('Home'), route('admin.home'));
});
require __DIR__.'/user.php';
require __DIR__.'/role.php';
require __DIR__.'/tip.php';
require __DIR__.'/employee.php';
require __DIR__.'/profile.php';
require __DIR__.'/customer.php';
require __DIR__.'/department.php';
require __DIR__.'/country.php';



