<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ProviderApiController;
use App\Http\Controllers\Api\UserApiController;
use App\Http\Controllers\Api\TipApiController;
use App\Http\Controllers\Api\CountryApiController;
use App\Http\Controllers\Api\PaymentApiController;







/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('/login_email', [AuthController::class, 'loginemail']);
    Route::post('/login_phone', [AuthController::class, 'loginphone']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user_profile', [AuthController::class, 'userProfile']);
    Route::post('/registercheck', [AuthController::class, 'registercheck']);

});

Route::group(['middleware' => ['auth:api']], function () {
    Route::post('/provider_information/{id}', [ProviderApiController::class, 'providerinformation']);
    Route::get('/all_partners/{name}', [ProviderApiController::class, 'allpartners']);   
    Route::post('/update_information', [UserApiController::class, 'updateinformation']);   
    Route::post('/update_info', [UserApiController::class, 'updateinfo']);   
    Route::post('/update_password', [UserApiController::class, 'updatepassword']);
    Route::post('/paytip/{id}/{payment_method_id}', [TipApiController::class, 'paytip']);
    Route::get('tips_history/{name}/{order}', [TipApiController::class, 'tipshistory']);   
    Route::get('/add_payment_method', [PaymentApiController::class, 'addpaymentmethod']);
    Route::get('/get_payment_methods', [PaymentApiController::class, 'getpaymentmethods']);
    Route::post('/delete_payment_method/{id}', [PaymentApiController::class, 'deletepaymentmethod']);
    Route::post('/pay_amount/{id}', [PaymentApiController::class, 'payamount']);



});
Route::post('/social_login', [ProviderApiController::class, 'sociallogin']);   
Route::get('/all_countries', [CountryApiController::class, 'allcountries']);   
Route::post('/thankyou', [\App\Http\Controllers\dashboard\PaymentController::class, 'thankyou']);
Route::post('/success', [\App\Http\Controllers\dashboard\PaymentController::class, 'success']);


