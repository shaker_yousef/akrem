<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

Route::get('/admin', [\App\Http\Controllers\dashboard\HomeController::class,'index'])->name('admin.home');
Route::resource('users',\App\Http\Controllers\dashboard\UserController::class);
Route::resource('roles',\App\Http\Controllers\dashboard\RoleController::class);
Route::resource('tips',\App\Http\Controllers\dashboard\TipController::class);
Route::resource('tipsemployee',\App\Http\Controllers\dashboard\TipEmployeeController::class);
Route::resource('tipspartnerdash',\App\Http\Controllers\dashboard\TipPartnerdashController::class);
Route::resource('tipspartnerdashpartneracc',\App\Http\Controllers\dashboard\PartneraccPartnerdashController::class);
Route::resource('employees',\App\Http\Controllers\dashboard\EmployeeController::class);
Route::resource('employeespartnerdash',\App\Http\Controllers\dashboard\EmployeePartnerdashController::class);
Route::resource('profile',\App\Http\Controllers\dashboard\ProfileController::class);
Route::resource('profileprovider',\App\Http\Controllers\dashboard\ProfileProviderController::class);
Route::resource('profileproviderempacc',\App\Http\Controllers\dashboard\ProfileProviderEmpAccController::class);
Route::resource('customers',\App\Http\Controllers\dashboard\CustomerController::class);
Route::resource('employeesadmdash',\App\Http\Controllers\dashboard\EmployeeAdmdashController::class);
Route::resource('employeesdashboard',\App\Http\Controllers\dashboard\EmployeedashboardController::class);
Route::get('feescontrol', [\App\Http\Controllers\dashboard\FeecontrolController::class, 'index'])
->name('feescontrol.index');
Route::post('feescontrol', [\App\Http\Controllers\dashboard\FeecontrolController::class, 'update'])
->name('feescontrol.update');

Route::resource('departments',\App\Http\Controllers\dashboard\DepartmentController::class);
Route::resource('countries',\App\Http\Controllers\dashboard\CountryController::class);




//Providers requests

Route::group(['namespace' => 'profile'], function (){
    Route::get('requests', [\App\Http\Controllers\dashboard\ProfileController::class, 'profileRequests'])
        ->name('profile.requests');
    Route::post('profileactive/{id}', [\App\Http\Controllers\dashboard\ProfileController::class, 'activeProfile'])
        ->name('profile.requests.active');
        Route::post('profiledeactive/{id}', [\App\Http\Controllers\dashboard\ProfileController::class, 'deactiveProfile'])
        ->name('profile.requests.deactive');
    Route::post('profileconfirm/{id}', [\App\Http\Controllers\dashboard\ProfileController::class, 'confirmProfile'])
        ->name('profile.requests.confirm');    
});


//Customers requests
Route::post('customeractivee/{id}', [\App\Http\Controllers\dashboard\CustomerController::class, 'activeCustomer'])
        ->name('customer.active');
Route::post('customerdeactivee/{id}', [\App\Http\Controllers\dashboard\CustomerController::class, 'deactiveCustomer'])
        ->name('customer.deactive');

        
//Employees requests AdminDash
Route::post('employeeadminactived/{id}/{partnerid}', [\App\Http\Controllers\dashboard\EmployeeController::class, 'activeEmployeeadmin'])
        ->name('employee.admin.active');
Route::post('employeeadmindeactived/{id}/{partnerid}', [\App\Http\Controllers\dashboard\EmployeeController::class, 'deactiveEmployeeadmin'])
        ->name('employee.admin.deactive');

//Employees requests PartnerDash
Route::post('employeepartneractived/{id}', [\App\Http\Controllers\dashboard\EmployeeController::class, 'activeEmployeepartner'])
        ->name('employee.partner.active');
Route::post('employeepartnerdeactived/{id}', [\App\Http\Controllers\dashboard\EmployeeController::class, 'deactiveEmployeepartner'])
        ->name('employee.partner.deactive');        

//Excel Employees
Route::get('file-import-export', [\App\Http\Controllers\dashboard\EmployeeController::class,'fileImportExport'])->name('file-import-export');
Route::post('file-import', [\App\Http\Controllers\dashboard\EmployeeController::class, 'fileImport'])->name('file-import');
Route::get('get/{filename}', [\App\Http\Controllers\dashboard\EmployeeController::class, 'getFile'])->name('getfile');
Route::post('search-export', [\App\Http\Controllers\dashboard\EmployeeController::class,'searchexport'])->name('search-export');

//Excel Tips Partner Account & Employee Account
Route::get('export-tips-partner-acc', [\App\Http\Controllers\dashboard\TipPartnerdashController::class,'exporttipspartneracc'])->name('export-tips-partner-acc');
Route::get('export-tips-employee-acc', [\App\Http\Controllers\dashboard\TipPartnerdashController::class,'exporttipsemployeeacc'])->name('export-tips-employee-acc');

//Tips PartnerDashboard Partner Account
Route::get('tipspartnerdashaccdate',[\App\Http\Controllers\dashboard\PartneraccPartnerdashController::class,'tipspartnerdashaccdate'])->name('partnerdash-partneracc-date-filter');

//Tips PartnerDashboard Employee Account
Route::get('tipsemployeedashaccdate',[\App\Http\Controllers\dashboard\TipPartnerdashController::class,'tipspartnerdashemployeeaccdate'])->name('partnerdash-employeeacc-date-filter');

//Tips EmployeeDashboard
Route::get('tipsemployeedashdate',[\App\Http\Controllers\dashboard\TipEmployeeController::class,'tipsemployeedashdate'])->name('employeedash-date-filter');


//Restpassword
Route::get('getupdatepassword', [\App\Http\Controllers\dashboard\ResetPasswordController::class, 'getupdatepassword'])->name('update.password.get');
Route::post('postupdatepassword', [\App\Http\Controllers\dashboard\ResetPasswordController::class, 'postupdatepassword'])->name('update.password.post'); 
//Restpassword-AdminDash-partners
Route::get('getupdatepassword_admindash_partners/{id}', [\App\Http\Controllers\dashboard\ResetPasswordController::class, 'getupdatepassword_admindash_partners'])->name('update.password.get.admindash.partners');
Route::post('postupdatepassword_partners', [\App\Http\Controllers\dashboard\ResetPasswordController::class, 'postupdatepassword_admindash_partners'])->name('update.password.post.admindash.partners'); 
//Restpassword-AdminDash-employees
Route::get('getupdatepassword_admindash_employees/{id}', [\App\Http\Controllers\dashboard\ResetPasswordController::class, 'getupdatepassword_admindash_employees'])->name('update.password.get.admindash.employees');
Route::post('postupdatepassword_employees', [\App\Http\Controllers\dashboard\ResetPasswordController::class, 'postupdatepassword_admindash_employees'])->name('update.password.post.admindash.employees'); 
//Restpassword-AdminDash-customers
Route::get('getupdatepassword_admindash_customers/{id}', [\App\Http\Controllers\dashboard\ResetPasswordController::class, 'getupdatepassword_admindash_customers'])->name('update.password.get.admindash.customers');
Route::post('postupdatepassword_customers', [\App\Http\Controllers\dashboard\ResetPasswordController::class, 'postupdatepassword_admindash_customers'])->name('update.password.post.admindash.customers'); 

//Update_Profile_Image
Route::get('updateprofileimage', [\App\Http\Controllers\dashboard\EmployeeController::class, 'updateprofileimage'])->name('update.profile.image');
Route::post('postupdateprofileimage/{id}', [\App\Http\Controllers\dashboard\EmployeeController::class, 'postupdateprofileimage'])->name('update.profile.image.post');

//ajax select
Route::get('/partner_department/{id}',  [\App\Http\Controllers\dashboard\EmployeeController::class, 'selectpartnerdepartment']);

//json
Route::get('/country_api',  [\App\Http\Controllers\dashboard\CountryController::class, 'country']);
