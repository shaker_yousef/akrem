@extends('layouts.dashboard.base')
@section('pageTitle', 'Fees control')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Fees control</h1>
                    </div>
                    <!-- <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            {{ Breadcrumbs::render('employees.index') }}
                        </ol>
                    </div> -->
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Fees control</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="form-group">
                                    <form id="editFee" action="{{ route('feescontrol.update') }}" method="post">
                                        @csrf
                                          <div class="form-group mb-4" style="max-width: 250px;
                                           /* margin: 0 auto; */
                                           ">
                                          
                                            <div class="form-group">
                                              <label for="exampleInputName3">Set Fees</label>
                                              <input value="{{ $fees->value }}" type="text" name="value" class="form-control"
                                                id="exampleInputName3" placeholder="Enter fees">
                                            </div>
                                           
                                            <button class="btn btn-success">Save</button>
                                          </div>
                                               
                                       </form>
                                      
                                       
                                </div>
                              


                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
   
@section('custom-script')
    <script>
        $(function() {
            $('#editFee').validate({
                rules: {
                    value: {
                        required: true,
                    },
                   

                },
                messages: {
                    value: {
                        required: "Please enter a value",
                    },
                    
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        });
    </script>
@endsection
@endsection

