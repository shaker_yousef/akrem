@extends('layouts.dashboard.base')
@section('pageTitle', 'Tips')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Tips</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            {{ Breadcrumbs::render('tips.index') }}
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">All Tips</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                            <form action="{{ route('partnerdash-employeeacc-date-filter') }}" method="GET">
                                   <div class="input-group mb-3">
                                        <input type="date" class="form-control" name="start_date">
                                        <input type="date" class="form-control" name="end_date">
                                        <button class="btn btn-primary" style="background:#00C4B3!important;border-color:#00C4B3!important" type="submit">GET</button>
                                   </div>
                                </form>
                            <div class="form-group">
                                  <a class="btn btn-primary" style="background:#00C4B3!important;border-color:#00C4B3!important" href="{{ route('export-tips-employee-acc') }}">Export Tips</a>
                            </div>     
                            @isset($tips_partner)
                               
                            @endisset
                            <br>
                            @isset($tips_employee)
                                
                            @endisset
                            <?php
                               $sum_partner=0;
                                foreach($tips_partner as $tip_partner){
                                    if($provider_fixed == 1){
                                         $sum_partner += $tip_partner->amount -$fees_partner - $fees_country;

                                    }else{
                                        $sum_partner += $tip_partner->amount - ($fees_partner/100) *($tip_partner->amount - $fees_country)- $fees_country;
                                    }
                                    }
                                ?>
                               

                                <?php
                               $sum_employee=0;
                                foreach($tips_employee as $tip_employee){
                                    if($provider_fixed == 1){
                                        $sum_employee += $tip_employee->amount -$fees_partner - $fees_country;
                                    }else{
                                        $sum_employee += $tip_employee->amount - ($fees_partner/100) *($tip_employee->amount - $fees_country)- $fees_country;
                                    }
                                    }
                                ?>
                                

                                <?php
                               $sum=0;
                                
                                $sum = $sum_partner + $sum_employee;
                                    
                                ?>
                                <label>Total Tips</label>
                                {{number_format($sum,2)}}
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Amount</th>
                                        <th>Department</th>
                                        <th>Date Time</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                           
                                    @isset($tips_partner)
                                    @foreach($tips_partner as $tip)
                                    <?php if($provider_fixed == 1){ ?>
                                        <tr>
                                        <td>{{$tip->amount -$fees_partner - $fees_country}}</td>
                                        <td>{{$tip->department}}</td>
                                        <td>{{$tip->date_time}}</td>
                                        
                                    </tr> 
                                    <?php }else{ ?>
                                        <tr>
                                        <td>{{$tip->amount - ($fees_partner/100) *($tip->amount - $fees_country)- $fees_country }}</td>
                                        <td>{{$tip->department}}</td>
                                        <td>{{$tip->date_time}}</td>
                                        
                                    </tr>
                                   <?php  } ?>
                                    
                                    @endforeach

                                    @endisset
                                    @isset($tips_employee)
                                    @foreach($tips_employee as $tip)

                                    <?php if($provider_fixed == 1){ ?>
                                        <tr>
                                        <td>{{$tip->amount -$fees_partner - $fees_country}}</td>
                                        <td>{{$tip->department}}</td>
                                        <td>{{$tip->date_time}}</td>
                                        
                                    </tr> 
                                    <?php }else{ ?>
                                        <tr>
                                        <td>{{$tip->amount - ($fees_partner/100) *($tip->amount - $fees_country)- $fees_country }}</td>
                                        <td>{{$tip->department}}</td>
                                        <td>{{$tip->date_time}}</td>
                                        
                                    </tr>
                                   <?php  } ?>
                                    @endforeach

                                    @endisset
                                   
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Amount</th>
                                        <th>Department</th>
                                        <th>Date Time</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
