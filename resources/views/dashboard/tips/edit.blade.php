@extends('layouts.dashboard.base')
@section('pageTitle', 'Edit tip ' . $tip->payment_method)
@section('custom-style')
    <style>
        .img-fluid {
            max-width: 100%;
            height: 138px;
        }
    </style>
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit tip {{ $tip->payment_method }}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            {{ Breadcrumbs::render('tips.edit', $tip->id) }}
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- jquery validation -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Edit tip {{ $tip->payment_method }}</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form id="editTip" action="{{ route('tips.update', $tip->id) }}"
                                  method="post" >
                                    @csrf
                                    @method("PUT")
                                    <div class="card-body">
                                        <div class="card-body">
                                           
                                            <div class="form-group">
                                                <label for="exampleInputName1">Amount</label>
                                                <input value="{{number_format($tip->amount,2)}}" type="text" name="amount" class="form-control"
                                                       id="exampleInputName1" placeholder="Enter Amount">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputName2">Payment Method</label>
                                                <input value="{{$tip->payment_method }}" type="text" name="payment_method" class="form-control"
                                                       id="exampleInputName2" placeholder="Enter Payment method">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputName3">Transaction Id</label>
                                                <input value="{{ $tip->transaction_id }}" type="text" name="transaction_id" class="form-control"
                                                       id="exampleInputName3" placeholder="Enter Transaction id">
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="exampleInputRole1">Select Tipper</label>
                                                <select name="tip_receiver" class="form-control select2bs4"
                                                        id="exampleInputRole1">
                                                    @foreach($users as $user)
                                                        @if($user->id == $user->user_id)
                                                            <option selected
                                                                    value="{{$user->id}}">{{$user->name}}</option>
                                                        @else
                                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputName4">Receiver</label>
                                                <input value="{{ $tip->receiver }}" type="text" name="receiver" class="form-control"
                                                       id="exampleInputName4" placeholder="Enter Receiver">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputName5">Date Time</label>
                                                <input value="{{ $tip->date_time }}" type="text" name="date_time" class="form-control"
                                                       id="exampleInputName5" placeholder="Enter Date Time">
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary"  style="background:#00C4B3!important">Update</button>
                                    </div>
                                </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (left) -->
                    <!-- right column -->
                    <div class="col-md-6">

                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@section('custom-script')
    <script>
        $(function () {
            $('#createEmployee').validate({
                rules: {
                    amount: {
                        required: true,
                    },
                    payment_method: {
                        required: true,
                    },
                    transaction_id: {
                        required: true,
                    },
                    user_id: {
                        required: true,
                    },
                    receiver: {
                        required: true,
                    },
                },
                messages: {
                    amount: {
                        required: "Please enter an amount",
                    },
                    payment_method: {
                        required: "Please enter a payment method",
                    },
                    transaction_id: {
                        required: "Please enter a transaction id",
                    },
                    user_id: {
                        required: "Please select a user",
                    },
                    receiver: {
                        required: "Please enter a receiver",
                    },

                },
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        });
    </script>
    <!-- Page specific script -->
    <script>
        $(function () {
            // Summernote
            $('#summernote').summernote()

            // CodeMirror
            CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
                mode: "htmlmixed",
                theme: "monokai"
            });
        })
    </script>
    <script>
        $(function () {
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });
        })

    </script>
@endsection
@endsection
