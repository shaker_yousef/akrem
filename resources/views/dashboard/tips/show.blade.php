@extends('layouts.dashboard.base')
@section('pageTitle', 'Tip-' . $tip->payment_method)
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Profile. {{ $tip->payment_method }}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            {{ Breadcrumbs::render('tips.show', $tip->id) }}
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- jquery validation -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Tip {{ $tip->payment_method }}</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form id="showOrder">
                                <div class="card-body">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="exampleInputName2">Amount</label>
                                            <input readonly value="{{number_format($tip->amount,2)}}" type="text" name="day"
                                                   class="form-control" id="exampleInputName2"
                                                   placeholder="Enter amount">
                                        </div>
                                       
                                        <div class="form-group">
                                            <label for="exampleInputName2">Payment Mthod</label>
                                            <input readonly value="{{ $tip->payment_method }}" type="text" name="hour"
                                                   class="form-control" id="exampleInputName2"
                                                   placeholder="Enter payment method">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Transaction Id</label>
                                            <input readonly value="{{ $tip->transaction_id }}" type="text" name="transaction_id" class="form-control"
                                                   id="exampleInputName2" placeholder="Enter transaction id">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputRole1">Select Tipper</label>
                                            <select name="user_id" class="form-control" id="exampleInputRole1">
                                                <option selected disabled value="{{$tip->user->id}}">{{$tip->user->name}}</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Receiver</label>
                                            <input readonly value="{{ $tip->receiver }}" type="text" name="receiver" class="form-control"
                                                   id="exampleInputName2" placeholder="Enter receiver">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Date Time</label>
                                            <input readonly value="{{ $tip->date_time }}" type="text" name="date_time" class="form-control"
                                                   id="exampleInputName2" placeholder="Enter date time">
                                        </div>
                                     
                                       
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (left) -->
                    <!-- right column -->
                    <div class="col-md-6">

                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
