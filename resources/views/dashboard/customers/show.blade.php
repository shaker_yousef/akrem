@extends('layouts.dashboard.base')
@section('pageTitle', 'Customer - ' . $customer->name)
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Profile. {{ $customer->name }}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            {{ Breadcrumbs::render('customers.show', $customer->id) }}
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- jquery validation -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Customer {{ $customer->name }}</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form id="showUser">
                                <div class="card-body">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email Address</label>
                                            <input readonly value="{{ $customer->email }}" type="email" name="email"
                                                   class="form-control" id="exampleInputEmail1" placeholder="Enter Email">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Name</label>
                                            <input readonly value="{{ $customer->name }}" type="text" name="full_name"
                                                   class="form-control" id="exampleInputName2"
                                                   placeholder="Enter Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName3">Phone Number</label>
                                            <input readonly value="{{ $customer->phone_number }}" type="text" name="phone_number"
                                                   class="form-control" id="exampleInputName3"
                                                   placeholder="Enter Phone Number">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName4">Country</label>
                                            <input readonly value="{{ $customer->country->name }}" type="text" name="country_id"
                                                   class="form-control" id="exampleInputName4"
                                                   placeholder="Enter Country">
                                        </div>
                                       
                                        
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (left) -->
                    <!-- right column -->
                    <div class="col-md-6">

                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
