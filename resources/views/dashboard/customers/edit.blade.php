@extends('layouts.dashboard.base')
@section('pageTitle', 'Edit customer ' . $customer->name)
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit customer {{ $customer->name }}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            {{ Breadcrumbs::render('customers.edit', $customer->id) }}
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- jquery validation -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Edit customer {{ $customer->name }}</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form id="editCustomer" action="{{ route('customers.update', $customer->id) }}" method="post">
                                @csrf
                                @method("PUT")
                                
                                <div class="card-body">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="exampleInputName2">Name</label>
                                            <input value="{{ $customer->name }}" type="text" name="name"
                                                   class="form-control" id="exampleInputName2"
                                                   placeholder="Enter Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email Address</label>
                                            <input  value="{{ $customer->email }}" type="email" name="email"
                                                   class="form-control" id="exampleInputEmail1"
                                                   placeholder="Enter Email">
                                        </div>
                                        <div class="form-group">
                                                <label for="exampleInputName3">Phone Number</label>
                                                <input value="{{ $customer->phone_number }}" type="text" name="phone_number" class="form-control"
                                                       id="exampleInputName3" placeholder="Enter Phone Number">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputRole1">Select Country</label>
                                            <select name="country_id" class="form-control" id="exampleInputRole1">
                                                @foreach($countries as $country)
                                                    @if($country->id == $customer->country_id)
                                                        <option selected value="{{$country->id}}">{{$country->name}}</option>
                                                    @else
                                                        <option  value="{{$country->id}}">{{$country->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                           </div>

                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info"  style="background:#00C4B3!important">Update</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (left) -->
                    <!-- right column -->
                    <div class="col-md-6">

                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@section('custom-script')
    <script>
        $(function () {
            $('#editCustomer').validate({
                rules: {
                    name: {
                        required: true,
         
                    },
                    email: {
                        required: true,
                        email: true,
                    },
                    phone_number: {
                        required: true,
         
                    },
                   
                },
                messages: {
                    name: {
                        required: "Please enter a name",
                       
                    },
                    email: {
                        required: "Please enter a email address",
                        email: "Please enter a valid email address"
                    },
                    phone_number: {
                        required: "Please enter a phone number",
                       
                    },
                    
                },
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        });
    </script>
@endsection
@endsection
