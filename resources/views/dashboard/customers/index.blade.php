@extends('layouts.dashboard.base')
@section('pageTitle', 'Customers')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Customers</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            {{ Breadcrumbs::render('customers.index') }}
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">All Customers</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone Number</th>
                                        <th>Country</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($customers as $customer)
                                    <tr>
                                        <td>{{$customer->name}}</td>
                                        <td>{{$customer->email}}</td>
                                        <td>{{$customer->phone_number}}</td>
                                        <td>{{$customer->country->name}}</td>
                                        <td>
                                            <div class="btn-group">
                                                 <a href="{{route('customers.edit',$customer->id)}}" type="button"
                                                   class="btn btn-default btn-flat">
                                                    <i class="fas fa-pen"></i>
                                                </a>
                                                <a href="{{route('customers.show',$customer->id)}}" type="button"
                                                   class="btn btn-default btn-flat">
                                                    <i class="fas fa-eye"></i>
                                                </a>
                                                <form method="post"
                                                      action="{{route('customers.destroy',$customer->id)}}">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-default btn-flat"><i
                                                            class="fas fa-trash"></i> </button>
                                                </form>
                                                <a href="{{route('update.password.get.admindash.customers',$customer->id)}}" type="button"
                                                   class="btn btn-default btn-flat">
                                                    <i class="fas fa-lock"></i>
                                                </a>
                                                @if($customer->is_active=='0')
                                                <div class="btn-group" style="margin-left:5px">
                                                    <form method="post"
                                                          action="{{route('customer.active',$customer->id)}}">
                                                        @csrf
                                                        @method('POST')
                                                        <button type="submit" class="btn btn-info" style="background:#00C4B3!important;border-color:#00C4B3!important">Activate </button>
                                                    </form>
                                                </div>
                                                @endif
                                                @if($customer->is_active=='1')
                                                <div class="btn-group" style="margin-left:5px">
                                                    <form method="post"
                                                          action="{{route('customer.deactive',$customer->id)}}">
                                                        @csrf
                                                        @method('POST')
                                                        <button type="submit" class="btn btn-info" style="background:#00C4B3!important;border-color:#00C4B3!important">Deactivate</button>
                                                    </form>
                                                </div>
                                                @endif
                                            </div> 
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone Number</th>
                                        <th>Country</th>
                                       <th>Actions</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
