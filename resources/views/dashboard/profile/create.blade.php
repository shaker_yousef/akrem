@extends('layouts.dashboard.base')
@section('pageTitle', 'Create new partner')
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->

<style>
    .select2-container--default .select2-selection--multiple .select2-selection__choice{
        background:#00C4B3!important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove{
        color: #fff !important;
    }
    .select2-container--default .select2-results__option--highlighted[aria-selected]{
        background-color: #00C4B3!important;
    }
    .select2-container--default .select2-results__option--highlighted[aria-selected], .select2-container--default .select2-results__option--highlighted[aria-selected]:hover{
        background-color: #00C4B3!important;
    }
    #btn-create-proforma {
    display: none;
    }
    #btn-create-proforma1 {
    display: none;
    }
    .card-title{
        margin-bottom:0!important;
    }
</style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Create Partner</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            {{ Breadcrumbs::render('profile.create') }}
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- jquery validation -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Create Partner</h3>
                            </div>

                            <!-- /.card-header -->
                            <!-- form start -->
                            <form id="createProfile" action="{{ route('profile.store') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                @method("POST")
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="exampleInputName1">Partner Name</label>
                                        <input value="{{ old('partner_name') }}" type="text" name="partner_name" class="form-control"
                                               id="exampleInputName1" placeholder="Enter Partner Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName2">Bank Details (Optional)</label>
                                        <input value="{{ old('bank_details') }}" type="text" name="bank_details"
                                            class="form-control" id="exampleInputName2" placeholder="Enter Bank Details">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName3">Bank Name</label>
                                        <input value="{{ old('bank_name') }}" type="text" name="bank_name" class="form-control"
                                               id="exampleInputName3" placeholder="Enter Bank Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName4">TRN</label>
                                        <input value="{{ old('trn') }}" type="text" name="trn" class="form-control"
                                               id="exampleInputName4" placeholder="Enter TRN">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName5">Transfer Customer Name</label>
                                        <input value="{{ old('transfer_customer_name') }}" type="text" name="transfer_customer_name" class="form-control"
                                               id="exampleInputName5" placeholder="Enter Transfer Customer Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName6">AC</label>
                                        <input value="{{ old('ac') }}" type="text" name="ac" class="form-control" 
                                               id="exampleInputName6" placeholder="Enter AC">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName7">IBAN</label>
                                        <input value="{{ old('iban') }}" type="text" name="iban" class="form-control" 
                                               id="exampleInputName7" placeholder="Enter IBAN">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName8">Swift Code</label>
                                        <input value="{{ old('swift_code') }}" type="text" name="swift_code" class="form-control"
                                               id="exampleInputName8" placeholder="Enter Swift Code">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName9">Company Registration Document</label>
                                        <input value="{{ old('company_registration_document') }}" type="file" name="company_registration_document" class="form-control"
                                               id="exampleInputName9" placeholder="Enter Company Registration Document">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName10">Phone Number</label>
                                        <input value="{{ old('phone_number') }}" type="text" name="phone_number" class="form-control"
                                               id="exampleInputName10" placeholder="Enter Phone Number">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName11">Mobile Number</label>
                                        <input value="{{ old('mobile_number') }}" type="text" name="mobile_number" class="form-control"
                                               id="exampleInputName11" placeholder="Enter Mobile Number">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Contact Email</label>
                                        <input type="email" name="contact_email" class="form-control" id="exampleInputEmail1"
                                               placeholder="Enter Contact Email">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Password</label>
                                        <input type="password" name="password" class="form-control"
                                               id="exampleInputPassword1" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPasswordConfirmation1">Password Confirmation</label>
                                        <input type="password" name="password_confirmation" class="form-control"
                                               id="exampleInputPasswordConfirmation1" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName12">Website</label>
                                        <input value="{{ old('website') }}" type="text" name="website" class="form-control"
                                               id="exampleInputName12" placeholder="Enter Website">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName13">Address</label>
                                        <input value="{{ old('address') }}" type="text" name="address" class="form-control"
                                               id="exampleInputName13" placeholder="Enter Address">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName14">Logo</label>
                                        <input value="{{ old('logo') }}" type="file" name="logo" class="form-control"
                                               id="exampleInputName14" placeholder="Enter Logo">
                                    </div>
                                    
                                    <div class="form-group">
                                                <label for="select_services"> Select Services</label>
                                                <select name="services[]" class="form-control" id="select_services" multiple>
                                                    <!-- <option selected disabled>Please Select Services</option> -->
                                                    @foreach ($services as $service)
                                                    <option value="{{$service->name}}">{{$service->name}}</option>
                                                    @endforeach
                                                </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName15">Credit Method </label>
                                        <select class="form-control" name="credit_method">
                                            <option value="Partner Account">Partner Account </option>
                                            <option value="Employee Account ">Employee Account </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputRole2">Select Country</label>
                                        <select name="country_id" class="form-control" id="exampleInputRole2">
                                            <option selected disabled>Please Select Country</option>
                                            @foreach($countries as $country)
                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group" >
                                        <label>Fees :</label>
                                        <div class="form-group" style="display: inline-table;margin-left:3rem">
                                            <input  type="radio" class="minimal" name="statusfilter" value="2" autocomplete="off" style="margin-right:4px">
                                            <label for="show">Fixed Amount</label>
                                        </div>
                                        <div class="form-group" style="display:inline-block; margin-left:9rem">
                                            <!-- <label></label> -->
                                            <input type="radio" class="minimal" name="statusfilter" value="1" autocomplete="off">
                                            <label for="hide">Percentage</label>
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-6" id="btn-create-proforma" >
                                            <div class="form-group" >
                                            <!-- <label>First Name</label> -->
                                            <input  class="form-control" type="text" name="percentage" placeholder="Enter Percentage">
                                            </div>
                                            
                                    </div>
                                    <div class="col-md-6" id="btn-create-proforma1" >
                                            <div class="form-group" >
                                            <!-- <label>First Name</label> -->
                                            <input  class="form-control" type="text" name="fixed_amount" placeholder="Enter fixed amount">
                                            </div>
                                            
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName16">Special Fees (Optional)</label>
                                        <input value="{{ old('special_fees') }}" type="text" name="special_fees"
                                            class="form-control" id="exampleInputName16" placeholder="Enter Special Fees">
                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary" style="background:#00C4B3!important;border-color:#00C4B3!important">Create</button>
                                </div>
                                <script>
                                    $('#select_services').select2({
                                        width: '100%',
                                        placeholder: "Select an Option",
                                        allowClear: true,
                                    });
                                </script>
                                <script>
                                $('input[name="statusfilter"]').on("change", function() {
                                const isEqualTo_2 = $(this).val() != 2;
                                $('#btn-create-proforma').toggle(isEqualTo_2);
                                });
                                </script> 
                                <script>
                                $('input[name="statusfilter"]').on("change", function() {
                                const isEqualTo_1 = $(this).val() != 1;
                                $('#btn-create-proforma1').toggle(isEqualTo_1);
                                });
                                </script>   
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (left) -->
                    <!-- right column -->
                    <div class="col-md-6">

                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@section('custom-script')
    <script>
        $(function() {
            $('#createProfile').validate({
                rules: {
                    partner_name: {
                        required: true,
                    },
                    bank_name: {
                        required: true,
                    },
                    trn: {
                        required: true,
                        digits: true,
                        minlength: 15,
                        maxlength: 15,
                    },
                    transfer_customer_name: {
                        required: true,
                    },
                    ac: {
                        required: true,
                        digits: true,
                        minlength: 10,
                        maxlength: 10,
                    },
                    iban: {
                        required: true,
                        minlength: 5,
                        maxlength: 34,
                    },
                    swift_code: {
                        required: true,
                    },
                    company_registration_document: {
                        required: true,
                    },
                    phone_number: {
                        required: true,
                        minlength: 5
                    },
                    mobile_number: {
                        required: true,
                        minlength: 5
                    },
                    contact_email: {
                        required: true,
                        email: true,
                    },
                    password: {
                        required: true,
                        minlength: 6
                    },
                    password_confirmation: {
                        required: true,
                        minlength: 6,
                        equalTo: "#exampleInputPassword1"
                    },
                    website: {
                        required: true,
                    },
                    address: {
                        required: true,
                    },
                    logo: {
                        required: true,
                    },
                    services: {
                        required: true,
                    },
                    credit_method: {
                        required: true,
                    },
                    statusfilter: {
                        required: true,
                    },
                    percentage: {
                        required: true,
                        digits: true,
                        max: 100,
                    },
                    fixed_amount: {
                        required: true,
                        digits: true,
                    },
                    

                },
                messages: {
                    partner_name: {
                        required: "Please enter a partner name",
                    },
                    bank_name: {
                        required: "Please enter a banke name",
                    },
                    trn: {
                        required: "Please enter a TRN",
                        digits: "Please enter numbers only",
                        minlength: "Please enter a valid TRN",
                        maxlength: "Please enter a valid TRN"
                    },
                    transfer_customer_name: {
                        required: "Please enter a transfer customer name",
                    },
                    ac: {
                        required: "Please enter an AC",
                        digits: "Please enter numbers only",
                        minlength: "Please enter a valid AC",
                        maxlength: "Please enter a valid AC"

                    },
                    iban: {
                        required: "Please enter an IBAN",
                        minlength: "Please enter a valid IBAN",
                        maxlength: "Please enter a valid IBAN"
                    },
                    swift_code: {
                        required: "Please enter a swift code",
                    },
                    company_registration_document: {
                        required: "Please enter a company registration document",
                    },
                    phone_number: {
                        required: "Please enter a phone number",
                        minlength: "Please enter a valid phone number"
                    },
                    mobile_number: {
                        required: "Please enter a mobile number",
                        minlength: "Please enter a valid mobile number"
                    },
                    email: {
                        required: "Please enter a email address",
                        email: "Please enter a valid email address"
                    },
                    password: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 6 characters long"
                    },
                    password_confirmation: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 6 characters long",
                        equalTo: "Password and Password Confirmation must be match"
                    },
                    website: {
                        required: "Please enter a website",
                    },
                    address: {
                        required: "Please enter an address",
                    },
                    logo: {
                        required: "Please enter a logo",
                    },
                    services: {
                        required: "Please enter services",
                    },
                    credit_method: {
                        required: "Please select a credit method",
                    },
                    statusfilter: {
                        required: "Please select option to enter fees",
                    },
                    percentage: {
                        digits: "Please enter numbers only",
                        max: "Your percentage must be between 0-100"

                    },
                    fixed_amount: {
                        digits: "Please enter numbers only",

                    },
                  
                   
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        });
    </script>

@endsection
@endsection

