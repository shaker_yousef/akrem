@extends('layouts.dashboard.base')
@section('pageTitle', 'Partners Requests')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Partner</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            {{ Breadcrumbs::render('profile.index') }}
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">All Partners</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="container" style="overflow:auto">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Partner Name</th>
                                                <th>Bank Details</th>
                                                <th>Bank Name</th>
                                                <th>TRN</th>
                                                <th>Transfer Customer Name</th>
                                                <th>AC</th>
                                                <th>IBAN</th>
                                                <th>Swift Code</th>
                                                <th>Company Registration Document</th>
                                                <th>Phone Number</th>
                                                <th>Mobile Number</th>
                                                <th>Contact Email</th>
                                                <th>Website</th>
                                                <th>Address</th>
                                                <th>Logo</th>
                                                <th>Services</th>
                                                <th>Credit Method</th>
                                                <th>Qr Code</th>
                                                <th>Country</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($profile_informations as $profile_information)
                                                <tr>
                                                    <td>{{ $profile_information->partner_name }}</td>
                                                    <td>{{ $profile_information->bank_details}}</td>
                                                    <td>{{ $profile_information->bank_name }}</td>
                                                    <td>{{ $profile_information->trn }}</td>
                                                    <td>{{ $profile_information->transfer_customer_name }}</td>
                                                    <td>{{ $profile_information->ac }}</td>
                                                    <td>{{ $profile_information->iban }}</td>
                                                    <td>{{ $profile_information->swift_code }}</td>
                                                    <td>
                                                        <a href="{{ asset('storage/files/company_registration_document/' . $profile_information->company_registration_document) }}"
                                                            download>
                                                            DOWNLOAD
                                                        </a>
                                                    </td>
                                                    <td>{{ $profile_information->phone_number }}</td>
                                                    <td>{{ $profile_information->mobile_number }}</td>
                                                    <td>{{ $profile_information->contact_email }}</td>
                                                    <td>{{ $profile_information->website }}</td>
                                                    <td>{{ $profile_information->address }}</td>
                                                    <td><img style="width: 50px!important;"
                                                            src="{{ asset('storage/images/partners/' . $profile_information->logo) }}" />
                                                    </td>
                                                    <td> {{ implode(', ', $profile_information->services) }}</td>
                                                    <td>{{ $profile_information->credit_method }}</td>
                                                    <td>
                                                        <a href="{{ asset('storage/images/' . $profile_information->qr_code_provider) }}"
                                                            download>
                                                            <img style="width: 50px!important;"
                                                                src="{{ asset('storage/images/' . $profile_information->qr_code_provider) }}" />
                                                        </a>
                                                    </td>
                                                    <td>{{ $profile_information->country->name }}</td>

                                                    <td>
                                                        <div class="btn-group">
                                                            <a href="{{ route('profile.edit', $profile_information->id) }}"
                                                                type="button" class="btn btn-default btn-flat">
                                                                <i class="fas fa-pen"></i>
                                                            </a>
                                                    </td>      
                                                    <td>
                                                        <div class="btn-group">
                                                            <form method="post"
                                                                action="{{ route('profile.requests.confirm', $profile_information->id) }}">
                                                                @csrf
                                                                @method('POST')
                                                                <button type="submit" class="btn btn-primary"
                                                                    style="background:#00C4B3!important">Activate </button>
                                                            </form>
                                                        </div>

                                                    </td>

                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Partner Name</th>
                                                <th>Bank Details</th>
                                                <th>Bank Name</th>
                                                <th>TRN</th>
                                                <th>Transfer Customer Name</th>
                                                <th>AC</th>
                                                <th>IBAN</th>
                                                <th>Swift Code</th>
                                                <th>Company Registration Document</th>
                                                <th>Phone Number</th>
                                                <th>Mobile Number</th>
                                                <th>Contact Email</th>
                                                <th>Website</th>
                                                <th>Address</th>
                                                <th>Logo</th>
                                                <th>Services</th>
                                                <th>Credit Method</th>
                                                <th>Qr Code</th>
                                                <th>Country</th>
                                                <th>Actions</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
