@extends('layouts.dashboard.base')
@section('pageTitle', 'Partner - ' . $profile_informations->partner_name)
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<style>
    .select2-container--default .select2-selection--multiple .select2-selection__choice{
        background:#00C4B3!important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove{
        color: #fff !important;
    }
    .select2-container--default .select2-results__option--highlighted[aria-selected]{
        background-color: #00C4B3!important;
    }
    .select2-container--default .select2-results__option--highlighted[aria-selected], .select2-container--default .select2-results__option--highlighted[aria-selected]:hover{
        background-color: #00C4B3!important;
    }
</style>
<style>
              .image-code{
                width: 100px;
                object-fit: contain;
                height: 100px;
              }
</style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> {{ $profile_informations->partner_name }} Details</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            {{ Breadcrumbs::render('profile.show', $profile_informations->id) }}
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- jquery validation -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">{{ $profile_informations->partner_name }} Details</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <!-- <form id="showPartner"> -->
                            <form id="showPartner" action="{{ route('update.password.get') }}" method="get">
                                    @csrf
                                    @method("POST")
                                <div class="card-body">

                                    <button type="submit" class="btn btn-primary" style="background:#00C4B3!important;border-color:#00C4B3!important">Update Password</button>
                                    
                                <div class="card-body">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="exampleInputName2">Partner Name</label>
                                            <input readonly value="{{ $profile_informations->partner_name }}" type="text" name="partner_name"
                                                   class="form-control" id="exampleInputName2"
                                                   placeholder="Enter Partner Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Bank Details</label>
                                            <input readonly value="{{ $profile_informations->bank_details }}" type="text" name="bank_details" class="form-control"
                                                   id="exampleInputName2" placeholder="Enter Bank Details">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Bank Name</label>
                                            <input readonly value="{{ $profile_informations->bank_name }}" type="text" name="bank_name"
                                                   class="form-control" id="exampleInputName2"
                                                   placeholder="Enter Bank Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">TRN</label>
                                            <input readonly value="{{ $profile_informations->trn }}" type="text" name="trn"
                                                   class="form-control" id="exampleInputName2"
                                                   placeholder="Enter TRN">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Transfer Customer Name</label>
                                            <input readonly value="{{ $profile_informations->transfer_customer_name }}" type="text" name="transfer_customer_name"
                                                   class="form-control" id="exampleInputName2"
                                                   placeholder="Enter Transfer Customer Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">AC</label>
                                            <input readonly value="{{ $profile_informations->ac }}" type="text" name="ac"
                                                   class="form-control" id="exampleInputName2"
                                                   placeholder="Enter AC">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">IBAN</label>
                                            <input readonly value="{{ $profile_informations->iban }}" type="text" name="iban"
                                                   class="form-control" id="exampleInputName2"
                                                   placeholder="Enter IBAN">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Swift Code</label>
                                            <input readonly value="{{ $profile_informations->swift_code }}" type="text" name="swift_code"
                                                   class="form-control" id="exampleInputName2"
                                                   placeholder="Enter Swift Code">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Company Registration Document</label><br>
                                            <a style="color:#00C4B3!important" href="{{asset('storage/files/company_registration_document/'.$profile_informations->company_registration_document)}}">
                                                DOWNLOAD
                                            </a>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Phone Number</label>
                                            <input readonly value="{{ $profile_informations->phone_number }}" type="text" name="phone_number"
                                                   class="form-control" id="exampleInputName2"
                                                   placeholder="Enter Phone Number">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Mobile Number</label>
                                            <input readonly value="{{ $profile_informations->mobile_number }}" type="text" name="mobile_number"
                                                   class="form-control" id="exampleInputName2"
                                                   placeholder="Enter Mobile Number">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Contact Email</label>
                                            <input readonly value="{{ $profile_informations->contact_email }}" type="email" name="contact_email"
                                                   class="form-control" id="exampleInputEmail1"
                                                   placeholder="Enter Contact Email">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Website</label>
                                            <input readonly value="{{ $profile_informations->website }}" type="text" name="website"
                                                   class="form-control" id="exampleInputName2"
                                                   placeholder="Enter Website">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Address</label>
                                            <input readonly value="{{ $profile_informations->address }}" type="text" name="address"
                                                   class="form-control" id="exampleInputName2"
                                                   placeholder="Enter Address">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Logo</label><br>
                                            <img class="image-code" src="{{asset('storage/images/partners/'.$profile_informations->logo)}}" />
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Services</label>
                                            <input readonly value="{{ implode(', ', $profile_informations->services)}}" type="text" name="services"
                                                   class="form-control" id="exampleInputName2"
                                                   placeholder="Enter Services">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputRole1"> Credit method</label>
                                            <select name="credit_method" class="form-control" id="exampleInputRole1">
                                                <option selected disabled value="{{$profile_informations->credit_method}}">{{$profile_informations->credit_method}}</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                        <label for="exampleInputName2">Qr code</label><br>
                                        <img class="image-code" src="{{asset('storage/images/'.$profile_informations->qr_code_provider)}}" />
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Country</label>
                                            <input readonly value="{{ $profile_informations->country->name }}" type="text" name="country_id"
                                                   class="form-control" id="exampleInputName2"
                                                   placeholder="Enter Country">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Special Fees</label>
                                            <input readonly value="{{ $profile_informations->special_fees }}" type="text" name="special_fees"
                                                   class="form-control" id="exampleInputName2"
                                                   placeholder="Enter Special Fees">
                                        </div>
                                    </div>
                                </div>
                                <script>
                                    $('#select_services').select2({
                                        width: '100%',
                                        placeholder: "Select an Option",
                                        allowClear: true,
                                    });
                                </script>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (left) -->
                    <!-- right column -->
                    <div class="col-md-6">

                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
