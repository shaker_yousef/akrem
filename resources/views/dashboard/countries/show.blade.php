@extends('layouts.dashboard.base')
@section('pageTitle', 'Country-' . $country->name)
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Country {{$country->name }}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                           {{ Breadcrumbs::render('countries.show', $country->id) }}
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- jquery validation -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Country {{ $country->name }}</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form id="showCountry">
                                <div class="card-body">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="exampleInputName2">Name</label>
                                            <input readonly value="{{ $country->name }}" type="text" name="name"
                                                   class="form-control" id="exampleInputName2" placeholder="Enter Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Flag</label><br>
                                            <img class="image-code" style="width: 150px!important;" src="{{asset('storage/images/countries/flag/'.$country->flag)}}" />
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Currency</label>
                                            <input readonly value="{{ $country->currency }}" type="text" name="name"
                                                   class="form-control" id="exampleInputName2" placeholder="Enter Currency">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Fees</label>
                                            <input readonly value="{{ $country->fees }}" type="text" name="name"
                                                   class="form-control" id="exampleInputName2" placeholder="Enter Fees">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (left) -->
                    <!-- right column -->
                    <div class="col-md-6">

                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
