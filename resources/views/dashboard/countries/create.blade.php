@extends('layouts.dashboard.base')
@section('pageTitle', 'Create New Country')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Create New Country</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            {{ Breadcrumbs::render('countries.create') }}
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- jquery validation -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Create New Country</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form enctype="multipart/form-data" id="createCountry" action="{{ route('countries.store') }}" method="post">
                                @csrf
                                @method("POST")
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="exampleInputName1">Name</label>
                                        <input value="{{ old('name') }}"  type="text" name="name" class="form-control" id="exampleInputName1"
                                               placeholder="Enter Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName2">Flag</label>
                                        <input value="{{ old('flag') }}" type="file" name="flag" class="form-control"
                                               id="exampleInputName2" placeholder="Enter flag">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName3">Currency</label>
                                        <input value="{{ old('currency') }}"  type="text" name="currency" class="form-control" id="exampleInputName3"
                                               placeholder="Enter Currency">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName4">Fees</label>
                                        <input value="{{ old('fees') }}"  type="text" name="fees" class="form-control" id="exampleInputName4"
                                               placeholder="Enter Fees">
                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info" style="background:#00C4B3!important;border-color:#00C4B3!important">Create</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (left) -->
                    <!-- right column -->
                    <div class="col-md-6">

                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@section('custom-script')
    <script>
        $(function() {
            $('#createDepartment').validate({
                rules: {
                    name: {
                        required: true,
                    },
                    flag: {
                        required: true,
                    },
                    currency: {
                        required: true,
                    },
                    fees: {
                        required: true,
                    },
                
                },
                messages: {
                    name: {
                        required: "Please enter a name",
                    },
                    flag: {
                        required: "Please enter a flag",
                    },
                    currency: {
                        required: "Please enter a currency",
                    },
                    fees: {
                        required: "Please enter fees",
                    },
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        });
    </script>
@endsection
@endsection
