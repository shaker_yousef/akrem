@extends('layouts.dashboard.base')
@section('pageTitle', 'Edit employee ' . $employee->full_name)
@section('custom-style')
    <style>
        .img-fluid {
            max-width: 100%;
            height: 138px;
        }
    </style>
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit employee {{ $employee->full_name }}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            {{ Breadcrumbs::render('employees.edit', $employee->id) }}
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- jquery validation -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Edit employee {{ $employee->full_name }}</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form id="editEmployee" action="{{ route('employees.update', $employee->id) }}"
                                  method="post" enctype="multipart/form-data">
                                    @csrf
                                    @method("PUT")
                                    <div class="card-body">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="exampleInputName2">Full Name</label>
                                                <input value="{{$employee->full_name }}" type="text" name="full_name" class="form-control"
                                                       id="exampleInputName2" placeholder="Enter Full name">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputName3">Work Title</label>
                                                <input value="{{ $employee->work_title }}" type="text" name="work_title" class="form-control"
                                                       id="exampleInputName3" placeholder="Enter Work Title">
                                            </div>
                                           <div class="form-group">
                                            <label for="exampleInputRole1">Select Department</label>
                                            <select name="department_id" class="form-control" id="exampleInputRole1">
                                                @foreach($departments as $department)
                                                    @if($department->id == $employee->department_id)
                                                        <option selected value="{{$department->id}}">{{$department->name}}</option>
                                                    @else
                                                        <option  value="{{$department->id}}">{{$department->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                           </div>
                                           <div class="form-group">
                                                <label for="exampleInputName4">ID Number</label>
                                                <input value="{{ $employee->id_number }}" type="text" name="id_number" class="form-control"
                                                       id="exampleInputName4" placeholder="Enter ID Number">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputName5">ID Front pic</label>
                                                <input value="{{ $employee->id_front_pic }}" type="file" name="id_front_pic" class="form-control"
                                                       id="exampleInputName5" placeholder="Enter ID Front pic">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputName6">ID Back pic</label>
                                                <input value="{{ $employee->id_back_pic }}" type="file" name="id_back_pic" class="form-control"
                                                       id="exampleInputName6" placeholder="Enter ID Back pic">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputName7">Bank Details (Optional)</label>
                                                <input value="{{ $employee->bank_details }}" type="text" name="bank_details" class="form-control"
                                                       id="exampleInputName7" placeholder="Enter Bank Details">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputName8">Bank Name</label>
                                                <input value="{{ $employee->bank_name }}" type="text" name="bank_name" class="form-control"
                                                       id="exampleInputName8" placeholder="Enter ID Bank Name">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputName9">Transfer Customer Name</label>
                                                <input value="{{ $employee->transfer_customer_name }}" type="text" name="transfer_customer_name" class="form-control"
                                                       id="exampleInputName9" placeholder="Enter Transfer Customer Name">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputName10">AC</label>
                                                <input value="{{ $employee->ac }}" type="text" name="ac" class="form-control"
                                                       id="exampleInputName10" placeholder="Enter AC">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputName11">IBAN</label>
                                                <input value="{{ $employee->iban }}" type="text" name="iban" class="form-control"
                                                       id="exampleInputName11" placeholder="Enter IBAN">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputName12">Swift Code</label>
                                                <input value="{{ $employee->swift_code }}" type="text" name="swift_code" class="form-control"
                                                       id="exampleInputName12" placeholder="Enter Swift Code">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputName13">Mobile Number</label>
                                                <input value="{{ $employee->mobile_number }}" type="text" name="mobile_number" class="form-control"
                                                       id="exampleInputName13" placeholder="Enter Mobile Number">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email</label>
                                                <input readonly value="{{ $employee->email }}" type="email" name="email"
                                                       class="form-control" id="exampleInputEmail1"
                                                       placeholder="Enter Email">
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleInputRole1">Select Credit Frequency</label>
                                            <select name="credit_frequency" class="form-control" id="exampleInputRole1">
                                                    @if($employee->credit_frequency == 'weekly')
                                                        <option selected value="{{$employee->credit_frequency}}">{{$employee->credit_frequency}}</option>
                                                        <option  value="bi-weekly">Bi-Weekly</option>
                                                        <option  value="monthly">Monthly</option>
                                                    @endif
                                                    @if($employee->credit_frequency == 'bi-weekly')
                                                    <option  value="weekly">Weekly</option>
                                                    <option selected value="{{$employee->credit_frequency}}">{{$employee->credit_frequency}}</option>
                                                        <option  value="monthly">Monthly</option>
                                                    @endif
                                                    @if($employee->credit_frequency == 'monthly')
                                                    <option value="weekly">Weekly</option>
                                                    <option  value="bi-weekly">Bi-Weekly</option>
                                                    <option selected value="{{$employee->credit_frequency}}">{{$employee->credit_frequency}}</option>
                                                    @endif
                                                        <!-- <option selected value="{{$employee->credit_frequency}}">{{$employee->credit_frequency}}</option> -->

                                                        <!-- <option value="weekly">Weekly</option>
                                                        <option value="bi-weekly">Bi-Weekly</option>
                                                        <option value="monthly">Monthly</option> -->
                                            </select>
                                           </div>
                                            <div class="form-group">
                                                <label for="exampleInputName14">Profile Image</label>
                                                <input value="{{ $employee->profile_image }}" type="file" name="profile_image" class="form-control"
                                                       id="exampleInputName14" placeholder="Enter Profile Image">
                                            </div>


                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-info"  style="background:#00C4B3!important">Update</button>
                                    </div>
                                </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (left) -->
                    <!-- right column -->
                    <div class="col-md-6">

                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@section('custom-script')
    <script>
        $(function () {
            $('#editEmployee').validate({
                rules: {
                    full_name: {
                        required: true,
                    },
                    work_title: {
                        required: true,
                    },
                    department_id: {
                        required: true,
                    },
                    id_number: {
                        required: true,
                    },
                    bank_name: {
                        required: true,
                    },
                    transfer_customer_name: {
                        required: true,
                    },
                    ac: {
                        required: true,
                        // minlength: 10,
                        // maxlength: 10,
                    },
                    iban: {
                        required: true,
                        // minlength: 5,
                        // maxlength: 34,
                    },
                    swift_code: {
                        required: true,
                    },
                    mobile_number: {
                        required: true,
                        // minlength: 5
                    },
                    email: {
                        required: true,
                        email: true,
                    },
                    password: {
                        required: true,
                        minlength: 6
                    },
                    password_confirmation: {
                        required: true,
                        minlength: 6,
                        equalTo: "#exampleInputPassword1"
                    },
                    credit_frequency: {
                        required: true,
                    },


                },
                messages: {
                    full_name: {
                        required: "Please enter a name",
                    },
                    work_title: {
                        required: "Please enter a work title",
                    },
                    department_id: {
                        required: "Please select a department",
                    },
                    id_number: {
                        required: "Please enter an id number",
                    },
                    id_front_pic: {
                        required: "Please enter an id front pic",
                    },
                    id_back_pic: {
                        required: "Please enter an id back pic",
                    },
                    bank_name: {
                        required: "Please enter a bank name",
                    },
                    transfer_customer_name: {
                        required: "Please enter a transfer customer name",
                    },
                    ac: {
                        required: "Please enter an AC",
                        // minlength: "Please enter a valid AC",
                        // maxlength: "Please enter a valid AC"
                    },
                    iban: {
                        required: "Please enter an IBAN",
                        // minlength: "Please enter a valid IBAN",
                        // maxlength: "Please enter a valid IBAN"
                    },
                    swift_code: {
                        required: "Please enter a swift code",
                    },
                    mobile_number: {
                        required: "Please enter a mobile number",
                        // minlength: "Please enter a valid mobile number"
                    },
                    email: {
                        required: "Please enter a email address",
                        email: "Please enter a valid email address"
                    },
                    password: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 6 characters long"
                    },
                    password_confirmation: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 6 characters long",
                        equalTo: "Password and Password Confirmation must be match"
                    },
                    credit_frequency: {
                        required: "Please select a credit frequency",
                    },
                    profile_image: {
                        required: "Please enter a profile image",
                    },


                },
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        });
    </script>

@endsection
@endsection
