@extends('layouts.dashboard.base')
@section('pageTitle', 'Employee - ' . $employee->full_name)
@section('content')
<style>
     .image-code{
                width: 100px;
                object-fit: contain;
                height: 100px;
              }
</style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Profile. {{ $employee->full_name }}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            {{ Breadcrumbs::render('employees.show', $employee->id) }}
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- jquery validation -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Employee {{ $employee->name }}</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form id="showEmployee">
                                <div class="card-body">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="exampleInputName2">Full Name</label>
                                            <input readonly value="{{ $employee->full_name }}" type="text" name="iban"
                                                   class="form-control" id="exampleInputName2"
                                                   placeholder="Enter Full Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Work Title</label>
                                            <input readonly value="{{ $employee->work_title }}" type="text" name="work_title"
                                                   class="form-control" id="exampleInputName2"
                                                   placeholder="Enter Work Title">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputRole1">Select Department</label>
                                            <select name="department_id" class="form-control" id="exampleInputRole1">
                                                <option selected disabled value="{{$employee->department->name}}">{{$employee->department->name}}</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">ID Number</label>
                                            <input readonly value="{{ $employee->id_number }}" type="text" name="id_number" class="form-control"
                                                   id="exampleInputName2" placeholder="Enter ID Number">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">ID Front pic</label><br>
                                            <img class="image-code" src="{{asset('storage/images/employees/id_front_pic/'.$employee->id_front_pic)}}" />
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">ID Back pic</label><br>
                                            <img class="image-code" src="{{asset('storage/images/employees/id_back_pic/'.$employee->id_back_pic)}}" />
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Bank Details</label>
                                            <input readonly value="{{ $employee->bank_details }}" type="text" name="bank_details" class="form-control"
                                                   id="exampleInputName2" placeholder="Enter Bank Details">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Bank Name</label>
                                            <input readonly value="{{ $employee->bank_name }}" type="text" name="bank_name" class="form-control"
                                                   id="exampleInputName2" placeholder="Enter Bank Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Transfer Customer Name</label>
                                            <input readonly value="{{ $employee->transfer_customer_name }}" type="text" name="transfer_customer_name" class="form-control"
                                                   id="exampleInputName2" placeholder="Enter Transfer Customer Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">AC </label>
                                            <input readonly value="{{ $employee->ac }}" type="text" name="ac" class="form-control"
                                                   id="exampleInputName2" placeholder="Enter AC ">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">IBAN </label>
                                            <input readonly value="{{ $employee->iban }}" type="text" name="iban" class="form-control"
                                                   id="exampleInputName2" placeholder="Enter IBAN ">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Swift Code</label>
                                            <input readonly value="{{ $employee->swift_code }}" type="text" name="swift_code" class="form-control"
                                                   id="exampleInputName2" placeholder="Enter Swift Code">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Mobile Number</label>
                                            <input readonly value="{{ $employee->mobile_number }}" type="text" name="mobile_number" class="form-control"
                                                   id="exampleInputName2" placeholder="Enter Mobile Number">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email</label>
                                            <input readonly value="{{ $employee->email }}" type="email" name="email"
                                                   class="form-control" id="exampleInputEmail1" placeholder="Enter Email">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputRole1">Select Credit Frequency</label>
                                            <select name="credit_frequency" class="form-control" id="exampleInputRole1">
                                                <option selected disabled value="{{$employee->credit_frequency}}">{{$employee->credit_frequency}}</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName2">Qr Code</label><br>
                                        <img class="image-code" src="{{asset('storage/images/'.$employee->qr_code)}}" />

                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (left) -->
                    <!-- right column -->
                    <div class="col-md-6">

                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
