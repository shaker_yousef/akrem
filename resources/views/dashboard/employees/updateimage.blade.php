@extends('layouts.dashboard.base')
@section('pageTitle', 'Update Profile Image ')
@section('custom-style')
    <style>
        .img-fluid {
            max-width: 100%;
            height: 138px;
        }
    </style>
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Update Profile Image</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            {{ Breadcrumbs::render('employees.edit', $employee->id) }}
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- jquery validation -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Update Profile Image</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form id="editEmployee" action="{{ route('update.profile.image.post', $employee->id) }}"
                                  method="post" enctype="multipart/form-data">
                                    @csrf
                                    @method("POST")
                                    <div class="card-body">
                                        <div class="card-body">
                                        <!-- <img class="image-code" src="{{asset('storage/images/employees/profile_image/'.$employee->profile_image)}}" /> -->
                                           
                                            <div class="form-group">
                                                <label for="exampleInputName13">New Profile Image</label>
                                                <input value="{{ $employee->profile_image }}" type="file" name="profile_image" class="form-control"
                                                       id="exampleInputName13" placeholder="Enter Profile Image">
                                            </div>


                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-info"  style="background:#00C4B3!important">Update</button>
                                    </div>
                                </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (left) -->
                    <!-- right column -->
                    <div class="col-md-6">

                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
