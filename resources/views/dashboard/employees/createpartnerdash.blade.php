@extends('layouts.dashboard.base')
@section('pageTitle', 'Create new employee')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Create new employee</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            {{ Breadcrumbs::render('employees.create') }}
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- jquery validation -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Create new employee</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form id="createEmployee" action="{{ route('employees.store') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                @method("POST")
                                 <div class="card-body">
                                
                                    <input value="{{$provider->id}}" type="text" name="partner" 
                                              hidden>
                                    <div class="form-group">
                                        <label for="exampleInputName1">Full Name</label>
                                        <input value="{{ old('full_name') }}" type="text" name="full_name" class="form-control"
                                               id="exampleInputName1" placeholder="Enter Full Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName2">Work Title</label>
                                        <input value="{{ old('work_title') }}" type="text" name="work_title" class="form-control"
                                               id="exampleInputName2" placeholder="Enter Work Title">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputRole2">Select Department</label>
                                        <select name="department_id" class="form-control" id="exampleInputRole2">
                                            <option selected disabled>Please Select Department</option>
                                            @foreach($departments as $department)
                                            <option value="{{$department->id}}">{{$department->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName3">ID Number</label>
                                        <input value="{{ old('id_number') }}" type="text" name="id_number" class="form-control"
                                               id="exampleInputName3" placeholder="Enter ID Number">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName4">ID Front pic</label>
                                        <input value="{{ old('id_front_pic') }}" type="file" name="id_front_pic" class="form-control"
                                               id="exampleInputName4" placeholder="Enter ID Front pic">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName5">ID Back pic</label>
                                        <input value="{{ old('id_back_pic') }}" type="file" name="id_back_pic" class="form-control"
                                               id="exampleInputName5" placeholder="Enter ID Back pic">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName6">Bank Details (Optional)</label>
                                        <input value="{{ old('bank_details') }}" type="text" name="bank_details"
                                            class="form-control" id="exampleInputName6" placeholder="Enter Bank Details">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName7">Bank Name</label>
                                        <input value="{{ old('bank_name') }}" type="text" name="bank_name" class="form-control"
                                               id="exampleInputName7" placeholder="Enter Bank Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName8">Transfer Customer Name</label>
                                        <input value="{{ old('transfer_customer_name') }}" type="text" name="transfer_customer_name" class="form-control"
                                               id="exampleInputName8" placeholder="Enter Transfer Customer Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName9">AC</label>
                                        <input value="{{ old('ac') }}" type="text" name="ac" class="form-control"
                                               id="exampleInputName9" placeholder="Enter AC">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName10">IBAN</label>
                                        <input value="{{ old('iban') }}" type="text" name="iban" class="form-control"
                                               id="exampleInputName10" placeholder="Enter IBAN">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName11">Swift Code</label>
                                        <input value="{{ old('swift_code') }}" type="text" name="swift_code" class="form-control"
                                               id="exampleInputName11" placeholder="Enter Swift Code">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName12">Mobile Number</label>
                                        <input value="{{ old('mobile_number') }}" type="text" name="mobile_number" class="form-control"
                                               id="exampleInputName12" placeholder="Enter Mobile Number">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email</label>
                                        <input type="email" name="email" class="form-control" id="exampleInputEmail1"
                                               placeholder="Enter Email">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Password</label>
                                        <input type="password" name="password" class="form-control"
                                               id="exampleInputPassword1" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPasswordConfirmation1">Password Confirmation</label>
                                        <input type="password" name="password_confirmation" class="form-control"
                                               id="exampleInputPasswordConfirmation1" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputRole3">Select Credit Frequency</label>
                                        <select name="credit_frequency" class="form-control" id="exampleInputRole3">
                                            <option selected disabled>Please Credit Frequency</option>
                                            <option value="weekly">Weekly</option>
                                            <option value="bi-weekly">Bi-Weekly</option>
                                            <option value="monthly">Monthly</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName13">Profile Image</label>
                                        <input value="{{ old('profile_image') }}" type="file" name="profile_image" class="form-control"
                                               id="exampleInputName13" placeholder="Enter Profile Image">
                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info" style="background:#00C4B3!important;border-color:#00C4B3!important">Create</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (left) -->
                    <!-- right column -->
                    <div class="col-md-6">

                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@section('custom-script')
    <script>
        $(function() {
            $('#createEmployee').validate({
                rules: {
                    partner: {
                        required: true,
                    },
                    full_name: {
                        required: true,
                    },
                    work_title: {
                        required: true,
                    },
                    department_id: {
                        required: true,
                    },
                    id_number: {
                        required: true,
                    },
                    id_front_pic: {
                        required: true,
                    },
                    id_back_pic: {
                        required: true,
                    },
                    bank_name: {
                        required: true,
                    },
                    transfer_customer_name: {
                        required: true,
                    },
                    ac: {
                        required: true,
                        // minlength: 10,
                        // maxlength: 10,
                    },
                    iban: {
                        required: true,
                        // minlength: 5,
                        // maxlength: 34,
                    },
                    swift_code: {
                        required: true,
                    },
                    mobile_number: {
                        required: true,
                        // minlength: 5
                    },
                    email: {
                        required: true,
                        email: true,
                    },
                    password: {
                        required: true,
                        // minlength: 6
                    },
                    password_confirmation: {
                        required: true,
                        // minlength: 6,
                        equalTo: "#exampleInputPassword1"
                    },
                    credit_frequency: {
                        required: true,
                    },
                    profile_image: {
                        required: true,
                    },
                    
                    
                },

               
                messages: {
                    partner: {
                        required: "Please select a partner",
                    },
                    full_name: {
                        required: "Please enter a name",
                    },
                    work_title: {
                        required: "Please enter a work title",
                    },
                    department_id: {
                        required: "Please select a department",
                    },
                    id_number: {
                        required: "Please enter an id number",
                    },
                    id_front_pic: {
                        required: "Please enter an id front pic",
                    },
                    id_back_pic: {
                        required: "Please enter an id back pic",
                    },
                    bank_name: {
                        required: "Please enter a bank name",
                    },
                    transfer_customer_name: {
                        required: "Please enter a transfer customer name",
                    },
                    ac: {
                        required: "Please enter an AC",
                        // minlength: "Please enter a valid AC",
                        // maxlength: "Please enter a valid AC"
                    },
                    iban: {
                        required: "Please enter an IBAN",
                        // minlength: "Please enter a valid IBAN",
                        // maxlength: "Please enter a valid IBAN"
                    },
                    swift_code: {
                        required: "Please enter a swift code",
                    },
                    mobile_number: {
                        required: "Please enter a mobile number",
                        // minlength: "Please enter a valid mobile number"
                    },
                    email: {
                        required: "Please enter a email address",
                        email: "Please enter a valid email address"
                    },
                    password: {
                        required: "Please provide a password",
                        // minlength: "Your password must be at least 6 characters long"
                    },
                    password_confirmation: {
                        required: "Please provide a password",
                        // minlength: "Your password must be at least 6 characters long",
                        equalTo: "Password and Password Confirmation must be match"
                    },
                    credit_frequency: {
                        required: "Please select a credit frequency",
                    },
                    profile_image: {
                        required: "Please enter a profile image",
                    },
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        });
    </script>
@endsection
@endsection
