@extends('layouts.dashboard.base')
@section('pageTitle', 'Employees')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Employees</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            {{ Breadcrumbs::render('employees.index') }}
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">All Employees</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                            <div class="container" style="overflow:auto">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Full Name</th>
                                        <th>Work Title</th>
                                        <th>Department</th>
                                        <th>ID Number</th>
                                        <th>ID Front pic</th>
                                        <th>ID Back pic </th>
                                        <th>Bank Details</th>
                                        <th>Bank Name</th>
                                        <th>Transfer Customer Name</th>
                                        <th>AC</th>
                                        <th>IBAN</th>
                                        <th>Swift Code</th>
                                        <th>Mobile Number</th>
                                        <th>Email</th>
                                        <th>Credit Frequency</th>
                                        <th>Profile Image</th>
                                        <th>Qr Code</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($employees as $employee)
                                    <tr>
                                        <td>{{$employee->full_name}}</td>
                                        <td>{{$employee->work_title}}</td>
                                        <td>{{$employee->department->name}}</td>
                                        <td>{{$employee->id_number}}</td>
                                        <td><img style="width: 50px!important;" src="{{asset('storage/images/employees/id_front_pic/'.$employee->id_front_pic)}}" /></td>
                                        <td><img style="width: 50px!important;" src="{{asset('storage/images/employees/id_back_pic/'.$employee->id_back_pic)}}" /></td>
                                        <td>{{$employee->bank_details}}</td>
                                        <td>{{$employee->bank_name}}</td>
                                        <td>{{$employee->transfer_customer_name}}</td>
                                        <td>{{$employee->ac}}</td>
                                        <td>{{$employee->iban}}</td>
                                        <td>{{$employee->swift_code}}</td>
                                        <td>{{$employee->mobile_number}}</td>
                                        <td>{{$employee->email}}</td>
                                       
                                        <td>{{$employee->credit_frequency}}</td>
                                        <td><img style="width: 50px!important;" src="{{asset('storage/images/employees/profile_image/'.$employee->profile_image)}}" /></td>
                                        <td>
                                        <a href="{{asset('storage/images/'.$employee->qr_code)}}" download>
                                        <img style="width: 50px!important;" src="{{asset('storage/images/'.$employee->qr_code)}}" />
                                        </a>
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="{{route('employees.edit',$employee->id)}}" type="button"
                                                   class="btn btn-default btn-flat">
                                                    <i class="fas fa-pen"></i>
                                                </a>
                                                <a href="{{route('employees.show',$employee->id)}}" type="button"
                                                   class="btn btn-default btn-flat">
                                                    <i class="fas fa-eye"></i>
                                                </a>
                                                <form method="post"
                                                      action="{{route('employees.destroy',$employee->id)}}">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-default btn-flat"><i
                                                            class="fas fa-trash"></i> </button>
                                                </form>
                                                <a href="{{route('update.password.get.admindash.employees',$employee->id)}}" type="button"
                                                   class="btn btn-default btn-flat">
                                                    <i class="fas fa-lock"></i>
                                                </a>
                                                @if($employee->is_active=='0')
                                                    <div class="btn-group" style="margin-left:5px">
                                                        <form method="post"
                                                            action="{{route('employee.partner.active',$employee->id)}}">
                                                            @csrf
                                                            @method('POST')
                                                            <button type="submit" class="btn btn-info" style="background:#00C4B3!important;border-color:#00C4B3!important">Activate </button>
                                                        </form>
                                                    </div>
                                                @endif
                                                @if($employee->is_active=='1')
                                                    <div class="btn-group" style="margin-left:5px">
                                                        <form method="post"
                                                            action="{{route('employee.partner.deactive',$employee->id)}}">
                                                            @csrf
                                                            @method('POST')
                                                            <button type="submit" class="btn btn-info" style="background:#00C4B3!important;border-color:#00C4B3!important">Deactivate</button>
                                                        </form>
                                                    </div>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                    <th>Full Name</th>
                                        <th>Work Title</th>
                                        <th>Department</th>
                                        <th>ID Number</th>
                                        <th>ID Front pic</th>
                                        <th>ID Back pic </th>
                                        <th>Bank Details</th>
                                        <th>Bank Name</th>
                                        <th>Transfer Customer Name</th>
                                        <th>AC</th>
                                        <th>IBAN</th>
                                        <th>Swift Code</th>
                                        <th>Mobile Number</th>
                                        <th>Email</th>
                                        <th>Credit Frequency</th>
                                        <th>Profile Image</th>
                                        <th>Qr Code</th>
                                        <th>Actions</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
