<aside class="main-sidebar sidebar-dark-primary elevation-4" >
    <!-- Brand Logo -->
    
    <img src="{{asset('assets/img/logo.png')}}" alt=" Logo" style="    background-color: white;
    padding: 10px;
    width: 100%;
    height: 56px;
    object-fit: contain;">

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            @if(auth()->user()->role->name=='employee')
            <div class="image">
                <img  src="{{asset('storage/images/employees/profile_image/'.$employee->profile_image)}}" class="img-circle elevation-2" alt="User Image">
            </div>
            @endif
            <div class="info">
                <a href="#" class="d-block">{{auth()->user()->name}}</a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
               
                @if(auth()->user()->role->name=='admin')
                <li class="nav-item ">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Admin Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('users.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Admin</p>
                            </a>
                            <a href="{{route('users.create')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Create New Admin</p>
                            </a>
                        </li>
                    </ul>
                </li>
                
                <li class="nav-item ">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user-lock"></i>
                        <p>
                            Partners Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('profile.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Partners</p>
                            </a>
                            <a href="{{route('profile.requests')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Partners Requests</p>
                            </a>
                            <a href="{{route('profile.create')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p> Create New Partner</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item ">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user-lock"></i>
                        <p>
                        Customers Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('customers.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Customers</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item ">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user-lock"></i>
                        <p>
                            Employees Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('employeesadmdash.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Employees</p>
                            </a>
                            <a href="{{route('employees.create')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p> Create New Employee</p>
                            </a>
                            <a href="{{route('file-import-export')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p> Upload employees </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item ">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Countries Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                   
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('countries.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Countries</p>
                            </a>
                            <a href="{{route('countries.create')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Create New Country</p>
                            </a>
                        </li>
                    </ul>
                  
                </li>
                @endif
                @if(auth()->user()->role->name=='provider')
                <?php  
                $user =auth()->user()->id;
                $user = App\Models\User::find($user);
                $partner = App\Models\Profile::where('contact_email',$user->email)->first();
                if($partner->credit_method=='Employee Account'){
                ?>  
                <li class="nav-item">
                    <a href="{{route('profileproviderempacc.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Profile
                        </p>
                    </a>
                </li>
                <?php
                  }else{
                ?>  
                <li class="nav-item">
                    <a href="{{route('profileprovider.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Profile
                        </p>
                    </a>
                </li>
                <?php   
                }
                ?> 
            
                <li class="nav-item ">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Departments Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                   
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('departments.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Departments</p>
                            </a>
                            <a href="{{route('departments.create')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Create New Department</p>
                            </a>
                        </li>
                    </ul>
                  
                </li>
                <?php  
                $user =auth()->user()->id;
                $user = App\Models\User::find($user);
                $partner = App\Models\Profile::where('contact_email',$user->email)->first();
                if($partner->credit_method=='Partner Account'){
                    ?>
               <li class="nav-item ">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Tips Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('tipspartnerdashpartneracc.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Tips</p>
                            </a>
                            
                        </li>
                    </ul>
                </li>
                <?php
                  }else{
                
                ?> 
                    <li class="nav-item ">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Tips Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('tipspartnerdash.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Tips</p>
                            </a>
                            
                        </li>
                    </ul>
                </li> 
                <?php   
                }
                ?> 
                <?php  
                $user =auth()->user()->id;
                $user = App\Models\User::find($user);
                $partner = App\Models\Profile::where('contact_email',$user->email)->first();
                if($partner->credit_method=='Employee Account'){
                    ?>
                    <li class="nav-item ">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Employees Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                   
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('employeespartnerdash.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Employees</p>
                            </a>
                            <a href="{{route('employeespartnerdash.create')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Create New Employee</p>
                            </a>
                        </li>
                    </ul>
                  
                </li>
                
               <?php
                  }
               ?> 
              
                 
                @endif
               
                @if(auth()->user()->role->name=='employee')
                <li class="nav-item">
                    <a href="{{route('employeesdashboard.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Personal Information
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('tipsemployee.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Tips History
                        </p>
                    </a>
                </li>
                @endif
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
</aside>
