<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
            'full_name'=> 'John Doe',
            'work_title'=> 'Worker',
            'department_id'=> '1',
            'id_number'=> '2423424243424324324',
            'id_front_pic' => 'dummy_1651588678.pdf',
            'id_back_pic' => 'dummy_1651588679.pdf',
            'bank_name'=> 'Mashreq Bank',
            'transfer_customer_name' => 'John Doe',
            'ac'=> '2231321312',
            'iban'=> '213213234234234345345353545353',
            'swift_code'=> '234324',
            'mobile_number'=>'232344242334',
            'email'=> 'john@akremapp.com',
            'password'=> Hash::make('123456'),
            'credit_frequency'=> 'weekly',
            'profile_image'=> 'images_1651588679.png',
            'provider_id' => '2',
            'qr_code'=> '_j4okxf.png',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()
        ]);
       
        
        
    }
}
