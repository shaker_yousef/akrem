<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=>'admin',
            'email'=>'admin@akrem.com',
            'email_verified_at'=>Carbon::now(),
            'password'=>Hash::make('secretakrem'),
            'role_id'=>1,
            'country_id'=> '1',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()
        ]);
        DB::table('users')->insert([
            'name'=>'Rotana Hotel',
            'email'=>'rotana@akremapp.com',
            'email_verified_at'=>Carbon::now(),
            'password'=>Hash::make('123456'),
            'role_id'=>3,
            'country_id'=> '2',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()
        ]);
        DB::table('users')->insert([
            'name'=>'Hilton',
            'email'=>'hilton@akremapp.com',
            'email_verified_at'=>Carbon::now(),
            'password'=>Hash::make('123456'),
            'role_id'=>3,
            'country_id'=> '1',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()
        ]);
        DB::table('users')->insert([
            'name'=>'John Doe',
            'email'=>'john@akremapp.com',
            'email_verified_at'=>Carbon::now(),
            'phone_number'=> '232344242334',
            'password'=>Hash::make('123456'),
            'role_id'=>4,
            'image' => 'images_1651588679.png',
            'country_id'=> '1',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()
        ]);
    }
}
