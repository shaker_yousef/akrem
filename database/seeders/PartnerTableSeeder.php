<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class PartnerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([
            'partner_name'=> 'Rotana Hotel',
            'bank_name'=> 'Mashreq Bank',
            'trn'=> '232424324242342',
            'transfer_customer_name'=> 'Rotana Hotel',
            'ac' => '2231321312',
            'iban' => '213213234234234345345353545353',
            'swift_code'=> '234324',
            'company_registration_document' => 'dummy_1651586514.pdf',
            'phone_number'=> '2132343534',
            'mobile_number'=> '2342345342',
            'contact_email'=> 'rotana@akremapp.com',
            'password'=>Hash::make('123456'),
            'website'=> 'www.example.com',
            'address'=> 'Dubai - Downtown',
            'logo'=> 'rotana-hotels-and-resorts-logo-vector_1650550398_1651586514.png',
            'services'=> '["F&B","Room Services"]',
            'credit_method' => 'Partner Account',
            'status'=> '1',
            'confirmation_status'=> '1',
            'qr_code_provider'=> '_DYlQzD.png',
            'country_id'=> '2',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()
        ]);
        DB::table('profiles')->insert([
            'partner_name'=> 'Hilton',
            'bank_name'=> 'Emirates NBD',
            'trn'=> '324342342234234',
            'transfer_customer_name'=> 'Hilton',
            'ac' => '3242324234',
            'iban' => '213213234234234345345353545353',
            'swift_code'=> '234324',
            'company_registration_document' => 'dummy_1651586724.pdf',
            'phone_number'=> '2131231312',
            'mobile_number'=> '232344242334',
            'contact_email'=> 'hilton@akremapp.com',
            'password'=>Hash::make('123456'),
            'website'=> 'https://example.com',
            'address'=> 'Jordan - Amman',
            'logo'=> 'HHR-Logo-Color_HR_1651586778.png',
            'services'=> '["All Services"]',
            'credit_method' => 'Employee Account',
            'status'=> '1',
            'confirmation_status'=> '1',
            'qr_code_provider'=> '_jIuK9L.png',
            'country_id'=> '1',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()
        ]);
        
        
    }
}
