<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            'name'=> 'Jordan',
            'flag'=> 'jordan_1651585822.png',
            'currency'=> 'JOD',
            'fees'=> '0.5',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()
        ]);
        DB::table('countries')->insert([
            'name'=> 'United Arab Emirates',
            'flag'=> 'united-arab-emirates_1651585877.png',
            'currency'=> 'AED',
            'fees'=> '0.5',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()
        ]);
        DB::table('countries')->insert([
            'name'=> 'Kuwait',
            'flag'=> 'kuwait_1651585921.png',
            'currency'=> 'KWD',
            'fees'=> '0.5',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()
        ]);
        DB::table('countries')->insert([
            'name'=> 'Saudi Arabia',
            'flag'=> 'saudi-arabia_1651585945.png',
            'currency'=> 'SAR',
            'fees'=> '0.5',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()
        ]);
        DB::table('countries')->insert([
            'name'=> 'Qatar',
            'flag'=> 'qatar_1651585982.png',
            'currency'=> 'QAR',
            'fees'=> '0.5',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()
        ]);
        DB::table('countries')->insert([
            'name'=> 'Bahrain',
            'flag'=> 'bahrain_1651586011.png',
            'currency'=> 'BHD',
            'fees'=> '0.5',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()
        ]);
        
    }
}
