<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            'name'=> 'Valet Parking',
            'partner_id'=> '2',
            'qr_code_department'=> 'Valet Services_nMrYaI.png',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()
        ]);
        DB::table('departments')->insert([
            'name'=> 'Room Services',
            'partner_id'=> '2',
            'qr_code_department'=> 'Room Services_cvRQhF.png',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()
        ]);
       
        
    }
}
