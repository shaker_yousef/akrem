<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('full_name');
            $table->string('work_title');
            $table->unsignedBigInteger('department_id')->nullable();
            $table->foreign('department_id')->on('departments')->references('id')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');
            $table->string('id_number');
            $table->string('id_front_pic');
            $table->string('id_back_pic');
            $table->string('bank_details')->nullable();
            $table->string('bank_name');
            $table->string('transfer_customer_name');
            $table->string('ac');
            $table->string('iban');
            $table->string('swift_code');
            $table->string('mobile_number');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('credit_frequency');
            $table->string('profile_image');
            $table->unsignedBigInteger('provider_id')->nullable();
            $table->foreign('provider_id')->on('profiles')->references('id')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');
            $table->text('qr_code')->nullable();
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });
    }
   
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
   
    }
}
