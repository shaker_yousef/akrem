<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->string('partner_name');
            $table->string('bank_details')->nullable();
            $table->string('bank_name');
            $table->string('trn');
            $table->string('transfer_customer_name');
            $table->string('ac');
            $table->string('iban');
            $table->string('swift_code');
            $table->string('company_registration_document');
            $table->string('phone_number');
            $table->string('mobile_number');
            $table->string('contact_email')->unique();
            $table->string('password');
            $table->string('website');
            $table->string('address');
            $table->string('logo');
            $table->string('services');
            $table->string('credit_method');
            $table->string('status')->default(0);
            $table->string('confirmation_status')->default(0);
            $table->text('qr_code_provider')->nullable();
            $table->unsignedBigInteger('country_id')->nullable();
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('CASCADE')
            ->onUpdate('CASCADE');
            $table->string('fees')->nullable();
            $table->boolean('is_fixed')->default(0);
            $table->string('special_fees')->nullable();
            $table->timestamps();
        });
    }
   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
