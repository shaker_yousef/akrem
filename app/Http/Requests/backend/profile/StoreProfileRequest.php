<?php

namespace App\Http\Requests\backend\profile;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class StoreProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'partner_name' => ['required', 'max:191'],
            'bank_name' => ['required', 'max:191'],
            'trn' => ['required','digits:15'],
            'transfer_customer_name' => ['required'],
            'ac' => ['required','digits:10'],
            'iban' => ['required','min:5','max:34'],
            'swift_code' => ['required'],
            'company_registration_document' => ['required'],
            'phone_number' => ['required'],
            'mobile_number' => ['required'],
            'contact_email' => ['required', 'email', 'max:191'],
            'password' => ['required', 'min:6', 'confirmed'],
            'website' => ['required'],
            'address' => ['required'],
            'logo' => ['required'],
            'services' => ['required'],
            'credit_method' => ['required'],
            'percentage' => ['max:100'],
       

        ];
    }
}
