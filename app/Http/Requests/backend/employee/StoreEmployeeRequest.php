<?php

namespace App\Http\Requests\backend\employee;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Auth;

class StoreEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // return [
        //     'full_name' => ['required', 'max:191'],
        //     'work_title' => ['required'],
        //     'department_id' => ['required'],
        //     'id_number' => ['required'],
        //     'id_front_pic' => ['required'],
        //     'id_back_pic' => ['required'],
        //     'bank_name' => ['required'],
        //     'transfer_customer_name' => ['required'],
        //     'ac' => ['required','digits:10'],
        //     'iban' => ['required','min:5','max:34'],
        //     'swift_code' => ['required'],
        //     'mobile_number' => ['required'],
        //     'email'  => ['required', 'email', 'max:191', 'unique:employees,email'],
        //     'password' => ['required', 'min:6', 'confirmed'],
        //     'credit_frequency' => ['required'],
        //     'profile_image' => ['required'],
        // ];


        return [
            'full_name' => ['required', 'max:191'],
            'work_title' => ['required'],
            'department_id' => ['required'],
            'id_number' => ['required'],
            'id_front_pic' => ['required'],
            'id_back_pic' => ['required'],
            'bank_name' => ['required'],
            'transfer_customer_name' => ['required'],
            'ac' => ['required'],
            'iban' => ['required'],
            'swift_code' => ['required'],
            'mobile_number' => ['required'],
            'email'  => ['required', 'email', 'max:191', 'unique:employees,email'],
            'password' => ['required'],
            'credit_frequency' => ['required'],
            'profile_image' => ['required'],
        ];
    }
}
