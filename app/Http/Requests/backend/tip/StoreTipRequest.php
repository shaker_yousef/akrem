<?php

namespace App\Http\Requests\backend\tip;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class StoreTipRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => ['required'],
            'payment_method' => ['required'],
            'transaction_id' => ['required'],
            'user_id' => ['required'],
            'receiver' => ['required'],
        ];
    }
}
