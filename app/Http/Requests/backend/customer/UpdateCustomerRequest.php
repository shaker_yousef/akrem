<?php

namespace App\Http\Requests\backend\customer;

use Illuminate\Foundation\Http\FormRequest;
use Auth;


class UpdateCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check() ;

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:191'],
            'email' => ['required', 'email', 'max:191'],
            'phone_number' => ['required'],
        ];
    }
}
