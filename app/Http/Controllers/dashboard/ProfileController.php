<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Profile;
use App\Repositories\backend\ProfileRepository;
use App\Http\Requests\backend\profile\UpdateProfileRequest;
use App\Http\Requests\backend\profile\StoreProfileRequest;
use App\Events\Backend\Profile\ProfileDeleted;
use App\Models\User;
use App\Models\Employee;
use App\Models\Country;
use App\Models\Service;
use Carbon\Carbon;


class ProfileController extends Controller
{
    public $profileRepository;

    public function __construct(ProfileRepository $profileRepository)
    {
        $this->profileRepository = $profileRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $profile_informations = Profile::where('status',1)->get();
        return view('dashboard.profile.index', compact('profile_informations'));
    }

    public function profileRequests()
    {
        $profile_informations = Profile::where('status',0)->get();
        return view('dashboard.profile.profileRequests', compact('profile_informations'));
    }

    public function activeProfile($id){
        $profile_informations=Profile::findOrFail($id);
        // $this->profileRepository->active($profile);
        $profile_informations->status=1;
        $profile_informations->save();
        session()->flash('success', __('Partner Activated'));
        return redirect()->route('profile.index');
    }
    public function confirmProfile($id){
        $profile=Profile::findOrFail($id);
        // $this->profileRepository->active($profile);
        $profile->confirmation_status=1;
        $profile->status=1;
        $profile->save();
        session()->flash('success', __('Partner Confirmed'));
        return redirect()->route('profile.index');
    }
    public function deactiveProfile($id){
        $profile_informations=Profile::findOrFail($id);
        $profile_informations->status=0;
        $profile_informations->save();
        session()->flash('success', __('Partner Deactivated'));
        return redirect()->route('profile.requests');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services =Service::all();
        $countries =Country::all();
        return view('dashboard.profile.create',compact('services','countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProfileRequest $request)
    {
        $this->profileRepository->create($request->only(
        'partner_name',
        'bank_details',
        'bank_name',
        'trn',
        'transfer_customer_name',
        'ac',
        'iban',
        'swift_code',
        'company_registration_document',
        'phone_number',
        'mobile_number',
        'contact_email',
        'password',
        'website',
        'address',
        'logo',
        'services',
        'credit_method',
        'country_id',
        'statusfilter',
        'percentage',
        'fixed_amount',
        'special_fees'
        ));

        session()->flash('success', __('Partner Created'));
        return redirect()->route('profile.requests');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile_informations = Profile::findOrFail($id);
        $services = Service::all();
        //return show view
        return view('dashboard.profile.show', compact('profile_informations','services'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $profile_informations = Profile::findOrFail($id);
         $services =Service::all();
         $countries =Country::all();
         $partner_services=implode(', ', $profile_informations->services); 
         return view('dashboard.profile.edit', compact('profile_informations','services','countries','partner_services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfileRequest $request, $id)
    {
        //find profile
        $profile=Profile::where('id',$id)->first();
        //update profile
        $this->profileRepository->update($profile,$request->only(
        'partner_name',
        'bank_details',
        'bank_name',
        'trn',
        'transfer_customer_name',
        'ac',
        'iban',
        'swift_code',
        'company_registration_document',
        'phone_number',
        'mobile_number',
        'contact_email',
        'website',
        'address',
        'logo',
        'services',
        'credit_method',
        'country_id',
        'statusfilter',
        'percentage',
        'fixed_amount',
        'special_fees'

        ));
        session()->flash('success', __('Partner Updated'));
        return redirect()->route('profile.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       //find profile
       $profile=Profile::findOrFail($id);
       //delete profile
       $profile->delete();
       $provider=User::where('email',$profile->contact_email)->first();
       $provider->delete();

       event(new ProfileDeleted($profile));
       session()->flash('success', __('Partner Deleted'));
       return redirect()->route('profile.index');
    }
}
