<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use App\Repositories\backend\UserRepository;
use App\Http\Requests\backend\user\StoreUserRequest;
use App\Http\Requests\backend\user\UpdateUserRequest;
use App\Events\Backend\User\UserDeleted;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index()
    { 
        $users = User::where('role_id',1)->get();
        return view('dashboard.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $this->userRepository->create($request->only(
            'name',
            'email',
            'password',
            'role_id'
        ));
        session()->flash('success', __('User Created'));
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find user
        $user = User::findOrFail($id);
        //return show view
        return view('dashboard.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //find user
        $user = User::findOrFail($id);
        //return edit view
        return view('dashboard.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
           //find user
           $user=User::where('id',$id)->first();
           //update user
           $this->userRepository->update($user,$request->only(
               'name',
               'email',
               'password',
               'role_id'
           ));
           session()->flash('success', __('User Updated'));
           return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          //find user
          $user=User::findOrFail($id);
          //delete user
          $user->delete();
          //event delete user
          event(new UserDeleted($user));
  
          session()->flash('success', __('User Deleted'));
          return redirect()->route('users.index');
    }
}
