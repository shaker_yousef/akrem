<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Department;
use App\Models\Profile;
use App\Models\User;
use App\Repositories\backend\DepartmentRepository;
use App\Http\Requests\backend\department\UpdateDepartmentRequest;
use App\Http\Requests\backend\department\StoreDepartmentRequest;
use App\Events\Backend\Department\DepartmentDeleted;




class DepartmentController extends Controller
{
    public $departmentRepository;

    public function __construct(DepartmentRepository $departmentRepository)
    {
    
        $this->departmentRepository = $departmentRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user= auth()->user()->id;
        $user = User::findOrFail($user);
        $partner = Profile::where('contact_email',$user->email)->first();
        $departments = Department::where('partner_id',$partner->id)->get();
        return view('dashboard.departments.index', compact('departments'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $partners = Profile::all();
        return view('dashboard.departments.create',compact('partners'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDepartmentRequest $request)
    {
       
        $this->departmentRepository->create($request->only(
            'name',
            'partner_id',
        ));
        session()->flash('success', __('Department Created'));
        return redirect()->route('departments.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
        $departments = Department::findOrFail($id);

        //return show view
        return view('dashboard.departments.show', compact('departments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $partners = Profile::all();
       //find department
       $department = Department::findOrFail($id);
       //return edit view
       return view('dashboard.departments.edit', compact('partners','department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDepartmentRequest $request, $id)
    {
       //find department
       $department=Department::where('id',$id)->first();
       //update department
       $this->departmentRepository->update($department,$request->only(
        'name',
        // 'partner_id',
       ));
       session()->flash('success', __('Department Updated'));
       return redirect()->route('departments.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       //find department
       $department=Department::findOrFail($id);
       //delete department
       $department->delete();
       //event delete department
       event(new DepartmentDeleted($department));
       session()->flash('success', __('Department Deleted'));
       return redirect()->route('departments.index');
    }
}
