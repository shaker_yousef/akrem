<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Repositories\backend\CountryRepository;
use App\Events\Backend\Country\CountryDeleted;
use Illuminate\Support\Facades\Http;
 

class CountryController extends Controller
{
    public $countryRepository;

    public function __construct(CountryRepository $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::all();
        return view('dashboard.countries.index', compact('countries'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        return view('dashboard.countries.create',compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->countryRepository->create($request->only(
            'name',
            'flag',
            'currency',
            'fees',
        ));
        session()->flash('success', __('Country Created'));
        return redirect()->route('countries.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $country = Country::findOrFail($id);

        //return show view
        return view('dashboard.countries.show', compact('country'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
       //find country
       $country = Country::findOrFail($id);
       //return edit view
       return view('dashboard.countries.edit', compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //find country
        $country=Country::where('id',$id)->first();
        //update country
        $this->countryRepository->update($country,$request->only(
            'name',
            'flag',
            'currency',
            'fees',   
        ));
        session()->flash('success', __('Country Updated'));
        return redirect()->route('countries.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find counrty
       $counrty=Country::findOrFail($id);
       //delete counrty
       $counrty->delete();
       //event delete counrty
       event(new CountryDeleted($counrty));
       session()->flash('success', __('Country Deleted'));
       return redirect()->route('countries.index');
    }

    function country() {
        $data =Http::get('https://stg.api.akremapp.com/api/all_countries')->json();
        return view('dashboard.json.country',['data' => $data]);

    }
}
