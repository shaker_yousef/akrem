<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Profile;
use App\Models\Department;
use App\Models\User;

class EmployeePartnerdashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provider=auth()->user()->id;
        $provider=User::findOrFail($provider);
        $provider=Profile::where('contact_email',$provider->email)->first();
        $employees = Employee::where('provider_id',$provider->id)->get();
        return view('dashboard.employees.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provider=auth()->user()->id;
        $provider=User::findOrFail($provider);
        $provider=Profile::where('contact_email',$provider->email)->first();
        $departments = Department::all();
        $partners = Profile::all();
        
        return view('dashboard.employees.createpartnerdash', compact('provider','departments','partners'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $partner = $request->input('partner');
        $employees=Employee::where('provider_id',$partner)->get();
        // dd($partner);
       
        $partners = Profile::all();

        return view('dashboard.employees.index', compact('employees'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
