<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tip;
use App\Models\Fee;
use App\Models\Country;
use App\Models\User;
use App\Models\Profile;
use Carbon\Carbon;

class PartneraccPartnerdashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provider=auth()->user()->id;
        $provider=User::findOrFail($provider);
        $provider=Profile::where('contact_email',$provider->email)->first();
        $provider_fixed = $provider->is_fixed;
        $fees_partner = $provider->fees;
        $country = Country::where('id',$provider->country_id)->first();
        $fees_country = $country->fees;
        $tips = Tip::where('receiver',$provider->partner_name)->get();
    
        //$totaltips = Tip::where('receiver',$provider->partner_name)->sum('amount');
        // $sum=0;
        // foreach($tips as $tip){
        //     $sum += $tip->amount;
        // }
       

        $fees = Fee::where('id',1)->first();
        $value = $fees->value;
        return view('dashboard.tips.totalamount', compact('provider_fixed','fees_partner','fees_country','tips'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function tipspartnerdashaccdate(Request $request)
    {
        $provider=auth()->user()->id;
        $provider=User::findOrFail($provider);
        $provider=Profile::where('contact_email',$provider->email)->first();
        if (request()->start_date || request()->end_date) {
            $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
            $end_date = Carbon::parse(request()->end_date)->toDateTimeString();
            $tips = Tip::where('receiver',$provider->partner_name)->whereBetween('date_time',[$start_date,$end_date])->get();
        } else {
            $tips = Tip::where('receiver',$provider->partner_name)::latest()->get();
        }
      
    
        $fees = Fee::where('id',1)->first();
        $value = $fees->value;
        return view('dashboard.tips.totalamount', compact('tips','value'));
    }
}
