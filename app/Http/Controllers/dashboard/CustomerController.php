<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Country;
use App\Models\User;
use App\Events\Backend\Customer\CustomerDeleted;
use App\Http\Requests\backend\customer\UpdateCustomerRequest;
use App\Http\Requests\backend\customer\StoreCustomerRequest;
use App\Repositories\backend\CustomerRepository;



class CustomerController extends Controller
{
    public $customerRepository;

    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $customers = User::where('role_id',2)->get();
        $customers = Customer::all();
  
        return view('dashboard.customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function activeCustomer($id){
        $customer=Customer::findOrFail($id);
        // $this->customerRepository->active($customer);
        $customer->is_active=1;
        $customer->save();
        session()->flash('success', __('Customer Activated'));
        return redirect()->route('customers.index');
    }

    public function deactiveCustomer($id){
        $customer=Customer::findOrFail($id);
        $customer->is_active=0;
        $customer->save();
        session()->flash('success', __('Customer Deactivated'));
        return redirect()->route('customers.index');
    }

    public function create()
    {
        $customers = Customer::all();
        return view('dashboard.customers.create',compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmployeeRequest $request)
    {
        $this->customerRepository->create($request->only(
            'name',
            'email',
            'phone_number',
            'country_id',
        ));
        session()->flash('success', __('Customer Created'));
        return redirect()->route('customers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $customer = Customer::findOrFail($id);
        $customer = Customer::findOrFail($id);

        //return show view
        return view('dashboard.customers.show', compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $countries = Country::all();

        //find customer
        $customer = Customer::findOrFail($id);
        //return edit view
        return view('dashboard.customers.edit', compact('countries','customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCustomerRequest $request, $id)
    {
       //find customer
       $customer=Customer::where('id',$id)->first();
       //update customer
       $this->customerRepository->update($customer,$request->only(
        'name',
        'email',
        'phone_number',
        'country_id',
       ));
       session()->flash('success', __('Customer Updated'));
       return redirect()->route('customers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       //find customer
       $customer=Customer::findOrFail($id);
       //delete customer
       $customer->delete();
       $user=User::where('email',$customer->email)->first();
       $user->delete();
       //event delete customer
       event(new CustomerDeleted($customer));
       session()->flash('success', __('Customer Deleted'));
       return redirect()->route('customers.index');
    }
}
