<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Fee;
use App\Models\User;
use Illuminate\Support\Facades\DB;



class FeecontrolController extends Controller
{
    public function index(){
        $fees = Fee::where('id', 1)->first();
       
        return view('dashboard.fees.index', compact('fees'));

    }

    public function update(Request $request){
        DB::table('fees')
              ->where('id', 1)
              ->update(['value' =>  $request['value']]);
        session()->flash('success', __('Fees Saved'));
        return redirect()->route('feescontrol.index');
    }
}
