<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth; 
use Hash;
use Illuminate\Support\Str;
use DB; 
use Carbon\Carbon; 
use App\Models\User;
use App\Models\Profile;
use App\Models\Employee; 
use App\Models\Customer; 


class ResetPasswordController extends Controller
{
    public function getupdatepassword(){
    $user=auth()->user();
    $employee=Employee::where('email',$user->email)->first();
    return view('dashboard.password.resetpassword',compact('employee'));
    }

    public function postupdatepassword(Request $request){
    
        $this->validate($request, [ 
            'current_password' => 'required',
            'new_password' => 'required',
        ]);
        $hashedPassword = Auth::user()->password;
        if (\Hash::check($request->current_password , $hashedPassword)) {
            
            if (!Hash::check($request->new_password , $hashedPassword)) {
                $users = User::find(Auth::user()->id);
                $partner = Profile::where('contact_email',$users->email)->first();
                if(!$partner){
                    $employee =Employee::where('email',$users->email)->first();
                    $employee->password = bcrypt($request->new_password);
                    $employee->save();
                    $users->password = bcrypt($request->new_password);
                    $users->save();
                    session()->flash('success','password updated successfully');
                    return redirect()->back();
                }else{
                    $partner->password = bcrypt($request->new_password);
                    $partner->save();
                    $users->password = bcrypt($request->new_password);
                    $users->save();
                    session()->flash('success','password updated successfully');
                    return redirect()->back();
                }
               
            }
            else{
                session()->flash('error',__('new password can not be the old password!'));
                return redirect()->back();
            } 
        }
        else{
           session()->flash('error', __('old password doesnt matched'));
           return redirect()->back();
        }
    }

    public function getupdatepassword_admindash_partners($id){
        $partner = Profile::findOrFail($id);
        return view('dashboard.password.reset_admindash_partners',compact('partner'));
    }
    public function postupdatepassword_admindash_partners(Request $request){
    
        $this->validate($request, [ 
            'new_password' => 'required',
        ]);
        $partner_id =$request->partner_id;
        $partner = Profile::findOrFail($partner_id);
        $users = User::where('email',$partner->contact_email)->first();
        $partner->password = bcrypt($request->new_password);
        //dd($partner->password);
        $partner->save();
        $users->password = bcrypt($request->new_password);
        $users->save();
        session()->flash('success','password updated successfully');
        return redirect()->back();
           
      
    }

    public function getupdatepassword_admindash_employees($id){
        $employee = Employee::findOrFail($id);
        return view('dashboard.password.reset_admindash_employees',compact('employee'));
    }
    public function postupdatepassword_admindash_employees(Request $request){
        $this->validate($request, [ 
            'new_password' => 'required',
        ]);
        $employee_id =$request->employee_id;
       
        $employee = Employee::findOrFail($employee_id);
        $users = User::where('email',$employee->email)->first();
        $employee->password = bcrypt($request->new_password);
        $employee->save();
        $users->password = bcrypt($request->new_password);
        $users->save();
        session()->flash('success','password updated successfully');
        return redirect()->back();
           
      
    }
    public function getupdatepassword_admindash_customers($id){
        $customer = Employee::findOrFail($id);
        return view('dashboard.password.reset_admindash_customers',compact('customer'));
    }
    public function postupdatepassword_admindash_customers(Request $request){
        $this->validate($request, [ 
            'new_password' => 'required',
        ]);
        $customer_id =$request->customer_id;
        $customer = Customer::findOrFail($customer_id);
        $customers = User::where('email',$customer->email)->first();
        $customers->password = bcrypt($request->new_password);
        $customers->save();
        session()->flash('success','password updated successfully');
        return redirect()->back();
           
      
    }
}
