<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tip;
use App\Models\User;
use App\Models\Fee;
use App\Repositories\backend\TipRepository;
use App\Http\Requests\backend\tip\StoreTipRequest;
use App\Http\Requests\backend\tip\UpdateTipRequest;
use App\Events\Backend\Tip\TipCreated;
use App\Events\Backend\Tip\TipUpdated;
use App\Events\Backend\Tip\TipDeleted;

class TipController extends Controller
{
    public $tipRepository;

    public function __construct(TipRepository $tipRepository)
    {
        $this->tipRepository = $tipRepository;
    }

    public function index()
    {
        $tips = Tip::all();
        $fees = Fee::where('id',1)->first();
        $value = $fees->value;
        return view('dashboard.tips.index', compact('tips','value'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tips = Tip::all();
        return view('dashboard.tips.create',compact('tips'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTipRequest $request)
    {
        $this->tipRepository->create($request->only(
            'amount',
            'payment_method',
            'transaction_id',
            'tipper',
            'receiver',
            'date_time',
        ));
        session()->flash('success', __('Tips Created'));
        return redirect()->route('tips.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         //find tip
         $tip = Tip::findOrFail($id);
         //return show view
         return view('dashboard.tips.show', compact('tip'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         //find tip
         $tip = Tip::findOrFail($id);
       
         $users = User::all();
         //return edit view
         return view('dashboard.tips.edit', compact('tip','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTipRequest $request, $id)
    {
        //find tip
        $tip=Tip::where('id',$id)->first();
        //update tip
        $this->tipRepository->update($tip,$request->only(
               'amount',
               'payment_method',
               'transaction_id',
               'user_id',
               'receiver',
               'date_time',
        ));
        session()->flash('success', __('Tips Updated'));
        return redirect()->route('tips.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       //find tip
       $tip=Tip::findOrFail($id);
       //delete tip
       $tip->delete();
       //event delete role
       event(new TipDeleted($tip));
       session()->flash('success', __('Tips Deleted'));
       return redirect()->route('tips.index');
    }
}
