<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Profile;
use App\Models\Department;
use App\Models\User;
use App\Http\Requests\backend\employee\StoreEmployeeRequest;
use App\Http\Requests\backend\employee\UpdateEmployeeRequest;
use App\Repositories\backend\EmployeeRepository;
use App\Events\Backend\Employee\EmployeeDeleted;
use App\Imports\EmployeesImport;
use App\Exports\EmployeesExport;
use App\Exports\EmployeesPartnerExport;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;


class EmployeeController extends Controller
{
    public $employeeRepository;

    public function __construct(EmployeeRepository $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    public function index()
    {
        $user=auth()->user();
        $provider=Profile::where('contact_email',$user->email)->first();
        $employees = Employee::where('provider_id',$provider->id)->get();
        return view('dashboard.employees.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function activeEmployeeadmin($id,$partnerid){
        $employee=Employee::findOrFail($id);
        // $this->employeeRepository->active($employee);
        $employee->is_active=1;
        $employee->save();
        $partner = $partnerid;
        $employees=Employee::where('provider_id',$partner)->get();
        $partners = Profile::where('credit_method','Employee Account')->get();
        session()->flash('success', __('Employee Activated'));
        return view('dashboard.employeesadmdash.index', compact('employees','partners','partner'));
    }

    public function deactiveEmployeeadmin($id,$partnerid){
        $employee=Employee::findOrFail($id);
        $employee->is_active=0;
        $employee->save();
        $partner = $partnerid;
        $employees=Employee::where('provider_id',$partner)->get();
        $partners = Profile::where('credit_method','Employee Account')->get();
        session()->flash('success', __('Employee Deactivated'));
        return view('dashboard.employeesadmdash.index', compact('employees','partners','partner'));
    }
    public function activeEmployeepartner($id){
        
        $employee=Employee::findOrFail($id);
        // $this->employeeRepository->active($employee);
        $employee->is_active=1;
        $employee->save();
        session()->flash('success', __('Employee Activated'));
        return redirect()->back();
    }

    public function deactiveEmployeepartner($id){
        $employee=Employee::findOrFail($id);
        $employee->is_active=0;
        $employee->save();
        session()->flash('success', __('Employee Deactivated'));
        return redirect()->back();
    }
    public function create()
    {
        $departments = Department::all();
        $partners = Profile::where('credit_method','Employee Account')->get();

        return view('dashboard.employees.create', compact('departments','partners'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmployeeRequest $request)
    {
        $this->employeeRepository->create($request->only(
            'full_name',
            'work_title',
            'department_id',
            'id_number',
            'id_front_pic',
            'id_back_pic',
            'bank_details',
            'bank_name',
            'transfer_customer_name',
            'ac',
            'iban',
            'swift_code',
            'mobile_number',
            'email',
            'password',
            'credit_frequency',
            'profile_image',
            'partner',
        ));

        session()->flash('success', __('Employee Created'));
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $departments = Department::all();
        $employee = Employee::findOrFail($id);
        //return show view
        return view('dashboard.employees.show', compact('employee','departments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departments = Department::all();
        //find employee
        $employee = Employee::findOrFail($id);
        //return edit view
        return view('dashboard.employees.edit', compact('departments','employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmployeeRequest $request, $id)
    {
       //find employee
       $employee=Employee::where('id',$id)->first();
       //update employee
       $this->employeeRepository->update($employee,$request->only(
            'full_name',
            'work_title',
            'department_id',
            'id_number',
            'id_front_pic',
            'id_back_pic',
            'bank_details',
            'bank_name',
            'transfer_customer_name',
            'ac',
            'iban',
            'swift_code',
            'mobile_number',
            'email',
            //'password',
            'credit_frequency',
            'profile_image',
            //'provider_id',
       ));
       session()->flash('success', __('Employee Updated'));
    //    return redirect()->route->('employees.index');
    return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

       //find employee
       $employee=Employee::findOrFail($id);

       //delete employee
       $employee->delete();
       $user=User::where('email',$employee->email)->first();
       $user->delete();
       //event delete employee
       event(new EmployeeDeleted($employee));
       session()->flash('success', __('Employee Deleted'));
       return redirect()->back();
    }


    public function fileImportExport()
    {
        $partners = Profile::where('credit_method','Employee Account')->get();
       return view('dashboard.employeesadmdash.file-import', compact('partners'));
    }

    /**
    * @return \Illuminate\Support\Collection
    */

    public function fileImport(Request $request)
    {
        $partner=$request->partner;
        Excel::import(new EmployeesImport($partner), $request->file('filename')->store('temp'));
        session()->flash('success', __('Employee Uploaded'));
        return back()->with('partner', $partner);

    }

    public function getFile($filename){
        $path = public_path($filename);
        return response()->download($path);
    }

    public function searchexport(Request $request) {
        $partner=$request->partner;
        switch ($request->input('action')){
            case 'search' :
                $partner = $request->input('partner');
                $employees=Employee::where('provider_id',$partner)->get();
                $partners = Profile::where('credit_method','Employee Account')->get();
                return view('dashboard.employeesadmdash.index', compact('employees','partners','partner'));

            case 'export_seclected_employees' :
                return Excel::download(new EmployeesPartnerExport($partner), 'employees.xlsx');

            case 'export_all_employees' :
                return Excel::download(new EmployeesExport, 'employees.xlsx');
        }
       
    }

    public function updateprofileimage() {
       $departments = Department::all();
       $id=auth()->user()->id;
       $user = User::findOrFail($id);
       $employee = Employee::where('email',$user->email)->first();
        return view('dashboard.employees.updateimage', compact('departments','employee'));
    }
    
    public function postupdateprofileimage(Request $request,$id){
        $employee=Employee::where('id',$id)->first();
       $this->employeeRepository->updateprofileimage($employee,$request->only(
        'profile_image',
       ));
       session()->flash('success', __('Employee Updated'));
       return redirect()->back();

    }

    public function selectpartnerdepartment($id)
    {
        $departments = DB::table("departments")->where("partner_id", $id)->pluck("name", "id");
        return json_encode($departments);
    }
    
}
