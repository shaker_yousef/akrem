<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tip;
use App\Models\Fee;
use App\Models\Employee;
use App\Models\Profile;
use App\Models\Country;
use Carbon\Carbon;


class TipEmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=auth()->user();
        $employee=Employee::where('email',$user->email)->first();
        $tips = Tip::where('employee_id',$employee->id)->get();
        $partner =Profile::where('id',$employee->provider_id)->first();
        $partner_fixed = $partner->is_fixed;
        $fees_partner = $partner->fees;
        $country = Country::where('id',$partner->country_id)->first();
        $fees_country = $country->fees;
        return view('dashboard.tips.indexempdash', compact('employee','tips','partner_fixed','fees_partner','fees_country'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function tipsemployeedashdate(Request $request)
    {
        $user=auth()->user();
        $employee=Employee::where('email',$user->email)->first();

        if (request()->start_date || request()->end_date) {
            $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
            $end_date = Carbon::parse(request()->end_date)->toDateTimeString();
            $tips = Tip::where('employee_id',$employee->id)->whereBetween('date_time',[$start_date,$end_date])->get();
        } else {
            $tips = Tip::where('employee_id',$employee->id)::latest()->get();
        }
      
        $fees = Fee::where('id',1)->first();
        $value = $fees->value;
        return view('dashboard.tips.indexempdash', compact('employee','tips','value'));
    }
}
