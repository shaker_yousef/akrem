<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\User;
use App\Models\PaymentMethod;
use Carbon\Carbon;
use App\Models\Tip;
use App\Models\Profile;
use App\Models\Employee;
use App\Models\Department;


class PaymentController extends Controller
{
   
  public function thankyou(Request $request){
    $token = $request->token;
    $tranRef = $request->tranRef;
    $token_payment = PaymentMethod::where('token',$token)->get();
    $email = $request->customerEmail;
    $user= User::where('email',$email)->first();
    
    if($token_payment->count() == 0){
      $query_token =[
        'profile_id' => '96282',
        'token' => $token,
      ];
      $result = Http::withHeaders([
        'authorization' => 'SHJND9KDBG-JDKKMLHZJ2-WRW9LNBLWD',
      ])
      ->post('https://secure.paytabs.com/payment/token', $query_token)->json();
     
      $payment_method=PaymentMethod::create([
        'user_id' => $user['id'],
        'email' => $email,
        'token' => $token,
        'card_type' => $result['payment_info']['card_type'],
        'card_scheme' => $result['payment_info']['card_scheme'],
        'payment_description' => $result['payment_info']['payment_description'],
        'tran_ref' => $tranRef,
  
      ]);
      $void =[
        'profile_id' => '96282',
        'tran_type' =>  'void',
        'tran_class' =>  'recurring',
        'tran_ref' =>  $tranRef,
        'token' =>   $token,
        'cart_id' =>  'cart_11111',
        'cart_currency' =>  'AED',
        'cart_amount' =>  1,
        'cart_description' =>  'Description of the items/services',
        'callback' =>  'https://webhook.site/4b3af623-085f-4b82-ab22-cb6cedeba218',
      ];
      $void_call = Http::withHeaders([
        'authorization' => 'SHJND9KDBG-JDKKMLHZJ2-WRW9LNBLWD',
      ])
      ->post('https://secure.paytabs.com/payment/request', $void)->json();
        return view('payment');
    }else{
      if(in_array($user->id, $token_payment->pluck('user_id')->toArray()) ){
        return response([
          'message' => 'already exist'
        ],400); 
      }else{
        $query_token =[
          'profile_id' => '96282',
          'token' => $token,
        ];
        $result = Http::withHeaders([
          'authorization' => 'SHJND9KDBG-JDKKMLHZJ2-WRW9LNBLWD',
        ])
        ->post('https://secure.paytabs.com/payment/token', $query_token)->json();
        $email = $request->customerEmail;
        $user= User::where('email',$email)->first();
        $payment_method=PaymentMethod::create([
          'user_id' => $user['id'],
          'email' => $email,
          'token' => $token,
          'card_type' => $result['payment_info']['card_type'],
          'card_scheme' => $result['payment_info']['card_scheme'],
          'payment_description' => $result['payment_info']['payment_description'],
          'tran_ref' => $tranRef,
    
        ]);
        $void =[
          'profile_id' => '96282',
          'tran_type' =>  'void',
          'tran_class' =>  'recurring',
          'tran_ref' =>  $tranRef,
          'token' =>   $token,
          'cart_id' =>  'cart_11111',
          'cart_currency' =>  'AED',
          'cart_amount' =>  1,
          'cart_description' =>  'Description of the items/services',
          'callback' =>  'https://webhook.site/4b3af623-085f-4b82-ab22-cb6cedeba218',
        ];
        $void_call = Http::withHeaders([
          'authorization' => 'SHJND9KDBG-JDKKMLHZJ2-WRW9LNBLWD',
        ])
        ->post('https://secure.paytabs.com/payment/request', $void)->json();
          return view('payment');
      }
    }  
  }

  public function success(Request $request){
    $cartId = $request->cartId;
    $cartId_arr = explode(',', $cartId);
    $email = $request->customerEmail;
    $user= User::where('email',$email)->first();
    if($cartId_arr[2] == 'E')
      {
        $employee = Employee::find($cartId_arr[0]);
        if(!$employee){
            return response([
                'message' => 'Invalid QR code'
            ],400);
          }else
          {
            $partner=Profile::where('id',$employee->provider_id)->first();
            $tip_currency = $partner->country->currency;
            $tip =[
                'amount' => floatval($cartId_arr[1]),
                'tipper' =>  $user->name,
                'receiver' => $partner->partner_name,
                'date_time' => Carbon::now(),
            ];    
            $data=Tip::create([
                'amount' => $tip['amount'],
                'payment_method' => 'Master Card',
                'transaction_id' => '',
                'user_id' => $user->id,
                'partner_id' =>  $partner->id,
                'department_id' =>  $employee->department_id,
                'employee_id' =>  $employee->id,
                'partner' =>  $partner->partner_name,
                'department' =>  $employee->department->name,
                'employee' =>  $employee->full_name,
                'receiver' => $tip['receiver'],
                'currency' => $tip_currency,
                'date_time' => Carbon::now(),
            ]);
            return response()->json($tip);
          }
      }elseif($cartId_arr[2] == 'P')
      {
        $partner = Profile::find($cartId_arr[0]);
        $tip_currency = $partner->country->currency;
        if(!$partner){
            return response([
                'message' => 'Invalid QR code'
            ],400);
        }else{
            $tip =[
                'amount' => floatval($cartId_arr[1]),
                'tipper' => $user->name,
                'receiver' => $partner->partner_name,
                'date_time' => Carbon::now(),
            ];
            $data=Tip::create([
                'amount' => $tip['amount'],
                'payment_method' => 'Master Card',
                'transaction_id' => '',
                'user_id' => $user->id,
                'partner_id' => $partner->id,
                'partner' => $partner->partner_name,
                'receiver' => $tip['receiver'],
                'currency' => $tip_currency,
                'date_time' => Carbon::now(),
            ]);
            return response()->json($tip);
          }
      }elseif($cartId_arr[2] == 'D')
      {
            $department = Department::find(floatval($cartId_arr[0]));
            if(!$department){
                return response([
                    'message' => 'Invalid QR code'
                ],400);
            }else{
                $partner =Profile::where('id',$department->partner_id)->first();
                $tip_currency = $partner->country->currency;
                $tip =[
                    'amount' => floatval($cartId_arr[1]),
                    'tipper' => $user->name,
                    'receiver' => $partner->partner_name,
                    'date_time' => Carbon::now(),
                ];
                $data=Tip::create([
                    'amount' => $tip['amount'],
                    'payment_method' => 'Master Card',
                    'transaction_id' => '',
                    'user_id' => $user->id,
                    'department_id' => $department->id,
                    'partner_id' => $partner->id,
                    'department' => $department->name,
                    'partner' => $partner->partner_name,
                    'receiver' => $tip['receiver'],
                    'currency' => $tip_currency,
                    'date_time' => Carbon::now(),
                ]);
                return response()->json($tip);
                }

      }

    
  }
}
