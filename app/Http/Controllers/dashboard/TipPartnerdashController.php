<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tip;
use App\Models\Fee;
use App\Models\User;
use App\Models\Profile;
use App\Models\Employee;
use App\Models\Country;
use App\Exports\TipsPartnerAccExport;
use App\Exports\TipsEmployeeAccExport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;



class TipPartnerdashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $provider=auth()->user()->id;
        $provider=User::findOrFail($provider);
        $provider=Profile::where('contact_email',$provider->email)->first();
        $provider_fixed = $provider->is_fixed;
        $fees_partner = $provider->fees;
        $country = Country::where('id',$provider->country_id)->first();
        $fees_country = $country->fees;
        $tips_partner = Tip::where('receiver',$provider->partner_name)->get();
        $employees = Employee::where('provider_id',$provider->id)->first();
        $tips_employee = Tip::where('receiver',$employees->full_name)->get();

    
        return view('dashboard.tips.index', compact('provider_fixed','fees_partner','fees_country','tips_partner','tips_employee'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function exporttipspartneracc(){
       
        $provider=auth()->user()->id;
        $provider=User::findOrFail($provider);
        $provider=Profile::where('contact_email',$provider->email)->first();
        $provider_fixed = $provider->is_fixed;
        $fees_partner = $provider->fees;
        $country = Country::where('id',$provider->country_id)->first();
        $fees_country = $country->fees;
        return Excel::download(new TipsPartnerAccExport($provider_fixed,$fees_partner,$fees_country), 'tips.xlsx');
    }

    public function exporttipsemployeeacc(){
        $provider=auth()->user()->id;
        $provider=User::findOrFail($provider);
        $provider=Profile::where('contact_email',$provider->email)->first();
        $provider_fixed = $provider->is_fixed;
        $fees_partner = $provider->fees;
        $country = Country::where('id',$provider->country_id)->first();
        $fees_country = $country->fees;
        return Excel::download(new TipsEmployeeAccExport($provider_fixed,$fees_partner,$fees_country), 'tips.xlsx');
    }

    public function tipspartnerdashemployeeaccdate(Request $request)
    {
        $provider=auth()->user()->id;
        $provider=User::findOrFail($provider);
        $provider=Profile::where('contact_email',$provider->email)->first();
        $employees = Employee::where('provider_id',$provider->id)->first();
        if (request()->start_date || request()->end_date) {
            $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
            $end_date = Carbon::parse(request()->end_date)->toDateTimeString();
            $tips_partner = Tip::where('receiver',$provider->partner_name)->whereBetween('date_time',[$start_date,$end_date])->get();
            $tips_employee = Tip::where('receiver',$employees->full_name)->whereBetween('date_time',[$start_date,$end_date])->get();
        } else {
            $tips_partner = Tip::where('receiver',$provider->partner_name)::latest()->get();
            $tips_employee = Tip::where('receiver',$employees->full_name)::latest()->get();
        }
      
        $fees = Fee::where('id',1)->first();
        $value = $fees->value;
        return view('dashboard.tips.index', compact('tips_partner','tips_employee','value'));
    }
}
