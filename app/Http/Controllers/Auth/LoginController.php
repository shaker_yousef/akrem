<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User;
use App\Models\Profile;
use App\Models\Employee;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
   
    protected function authenticated()
    {
        if(auth()->user()->role->name=='admin')
        {
            $users = User::where('role_id',1)->get();
            return response()->view('dashboard.users.index', compact('users'));
        }
        elseif(auth()->user()->role->name=='provider'){
            $user =auth()->user()->id;
            $user = User::find($user);
            $partner = Profile::where('contact_email',$user->email)->first();
            if($partner->credit_method=='Employee Account'){
                $provider=auth()->user()->id;
                $provider=User::findOrFail($provider);
                $profile_informations=Profile::where('contact_email',$provider->email)->first();
                if($profile_informations->status == 0 ){
                    Auth::logout();
                    return view('auth.login');
                }else{
                    return view('dashboard.profile.showempacc', compact('profile_informations'));
                }
            }else{
                $provider=auth()->user()->id;
                $provider=User::findOrFail($provider);
                $profile_informations=Profile::where('contact_email',$provider->email)->first();
                if($profile_informations->status == 0 ){
                    Auth::logout();
                    return view('auth.login');
                }else{
                    return view('dashboard.profile.show', compact('profile_informations'));

                }
            }
        }

        elseif(auth()->user()->role->name=='employee'){
            $employee=auth()->user()->id;
            $employee=User::findOrFail($employee);
            $employee=Employee::where('email',$employee->email)->first();
            if($employee->is_active == 0){
                Auth::logout();
                return view('auth.login');
            }else{
                return view('dashboard.employees.showempdash', compact('employee'));

            }
            }
    }
}
