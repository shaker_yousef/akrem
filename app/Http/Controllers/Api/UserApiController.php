<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\backend\api\UpdateInfoRequest;
use Illuminate\Validation\Rule; 


class UserApiController extends Controller
{
    public function updateinformation(Request $request){
       
        $user=auth("api")->user();
        $user=User::find($user->id);
        if($request->phone_number != ''){
            if($request->email != ''){
            if($request->phone_number == $user->phone_number){
                $validator = Validator::make($request->all(), [
                    'email' => [Rule::unique('users')->ignore($user->id)],
                    'image' => 'nullable|image'
                ]);
                if($validator->fails()){
                    return response()->json($validator->errors(), 400);
                }
            }
            $validator = Validator::make($request->all(), [
                'email' => [Rule::unique('users')->ignore($user->id)],
                // 'email' => 'required|email|unique:users,email,'.$user->id,
                'phone_number' => [Rule::unique('users')->ignore($user->id)],
                'image' => 'nullable|image'
            ]);
            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }
        }else{
            if($request->phone_number == $user->phone_number){
                $validator = Validator::make($request->all(), [
                    'image' => 'nullable|image'
                ]);
                if($validator->fails()){
                    return response()->json($validator->errors(), 400);
                }
            }
            $validator = Validator::make($request->all(), [
                'phone_number' => [Rule::unique('users')->ignore($user->id)],
                'image' => 'nullable|image'
            ]);
            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }
        }
        }else{
            if($request->email != ''){

            $validator = Validator::make($request->all(), [
                'email' => [Rule::unique('users')->ignore($user->id)],
                'image' => 'nullable|image'
            ]);
            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }
        }else{
            $validator = Validator::make($request->all(), [
                'image' => 'nullable|image'
            ]);
            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }  
        }
        }
           
        $name =$request->get('name');
        $email =$request->get('email');
        $phone_number =$request->get('phone_number');
        if($request->image && $request->image->isValid()){
            $file_name = time().'.'.$request->image->extension();
            $request->image->move(public_path('images'),$file_name);
            $path = "public/images/$file_name";
            $user->image = $path;
        }
        $country_id =$request->get('country_id');
        if($name != ''){
                $user->name = $name;
                $update_name = 'update';
        }
        else{
            $update_name = '';
        }
        if($email != ''){
            $user->email = $email;
            $update_email = 'update';

            }
            else{
                $update_email = '';
            }
        if($phone_number != ''){
            $user->phone_number = $phone_number;
            $update_phone = 'update';

        }
        else{
            $update_phone = '';
        }
        if($country_id != ''){
            $user->country_id = $country_id;
            $update_country = 'update';

        }
        else{
            $update_country = '';
        }
        $user->save();
        if($update_name !=null || $update_email !=null || $update_phone != null || $update_country != null){
            return response()->json([
                'message' => 'User successfully Updated',
                'user' => $user ,
            ]);
        }
        else{
            return response()->json([
                'message' => 'No update',
                'user' => $user ,
            ]); 
        }
    }
    public function updatepassword(Request $request){
        $validator = Validator::make($request->all(), [
            'current_password' => 'required',
            'new_password' => 'required',
            'retype_new_password' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $user=auth("api")->user();
        if(Hash::check($request->current_password,$user->password)){
            if($request->current_password!=$request->new_password){
                if($request->new_password==$request->retype_new_password){
                    $user->password=Hash::make($request['new_password']);
                    $user->save();
                    return response()->json(['message' => 'Password Changed']);
                }else{
                    return response([
                        'message' => 'You must enter the same password to confirm it'
                      ],400);
                }
            }
           else{
            return response([
                'message' => 'Password must differ from old password'
              ],400);   
           }
        }
       else {
        return response([
            'message' => 'you entered wrong password'
          ],400); 
       }
    }
} 
