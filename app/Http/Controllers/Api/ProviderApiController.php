<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\Employee;
use App\Models\Department;
use App\Models\Customer;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Fee;
use JWTAuth;
use Carbon\Carbon;
use App\Models\Country;
use Illuminate\Support\Facades\Validator;



class ProviderApiController extends Controller
{
    public function providerinformation(Request $request,$id){
        if($request->employee == 'E' && $request->partner == null && $request->department == null){
            $employee = Employee::find($id);
            if(!$employee){
                return response([
                    'message' => 'Invalid QR code'
                ],400);
            }
           
            else{
            $department = Department::where('id',$employee->department_id)->first();
            $partner= Profile::where('id',$employee->provider_id)->first();
            $fees=Fee::first();
                return response()->json([
                    'employee' => $employee,
                    'department' => $department,
                    'partner' => $partner,
                    'fees' => $partner->country->fees,
                ]);
            }
        }
        if($request->employee == null && $request->partner == 'P' && $request->department == null){
           $partner= Profile::find($id);
            if(!$partner){
                return response([
                    'message' => 'Invalid QR code'
                ],400);
            }
            else{
            $fees=Fee::first();
                return response()->json([
                    'partner' => $partner,
                    'fees' => $partner->country->fees,
                ]);
            }
        }
        if($request->employee == null && $request->partner == null && $request->department == 'D'){
            $department= Department::find($id);
             if(!$department){
                 return response([
                     'message' => 'Invalid QR code'
                 ],400);
             }
             else{
           $partner= Profile::where('id',$department->partner_id)->first();
           $fees=Fee::first();
                 return response()->json([
                    'department' => $department,
                    'partner' => $partner,
                    'fees' => $partner->country->fees,
                 ]);
             }
         }else{
            return response([
                'message' => 'Invalid QR code'
            ],400);
         }
    

    }
    public function allpartners($name){
        $user = auth()->user();
        $partners=Profile::all();
        if($name == 'all'){
            $partners=Profile::where('country_id',$user->country_id)->get();
            if(!$partners){
                return response()->json(['Result' => 'No Data not found'], 404);
            }
            else{
            return response()->json([
                'partners' => $partners
            ]);
            }
        }
        else{
        }
            $result = Profile::where('partner_name', 'LIKE', '%'. $name. '%')
            ->where('country_id',$user->country_id)
            ->get();
            if(count($result)){
             return Response()->json([
                'partners' => $result
            ]);
            }
            else
            {
            return response()->json(['Result' => 'No Data not found'], 404);
          }
    }
    
    public function sociallogin(Request $request)
    {
      
        $fake_mail = "0".$request->account_id."@gmail.com";
        try {
           
            $column = '';
            if ($request->type_account == 'google') {
                $column = 'google_id';
            }
            if ($request->type_account == 'facebook') {
                $column = 'facebook_id';
            }
            if ($request->type_account == 'apple') {
                $column = 'apple_id';
            }
           

            $data = [
                'name' => $request->first_name.$request->last_name,
                'email' => $request->email === null? $fake_mail :$request->email ,
                'password' => Hash::make(\Str::random(8)),
                'phone_number' => $request->phone_number,
                'country_id' => $request->country_id,
                'email_verified_at' => Carbon::now(),
                $column => $request->account_id
                ];
            $user = User::where($column,$request->account_id)->first();
            $country= Country::where('id',$request->country_id)->first();

            if (!is_null($user)) {
                $country= Country::where('id',$user->country_id)->first();
                $msg = "User Updated";
                $code = '200';
                $token = JWTAuth::fromUser($user);
            }
            else {
                $validator = Validator::make($request->all(), [
                    'phone_number' => ['nullable','unique:users,phone_number'],
                    'email' => ['nullable', 'unique:users,email', 'email'],
                    'country_id' => 'required',
                ]);
                if ($validator->fails()) {
                    return response()->json($validator->errors(), 422);
                }
                $user = User::query()->create($data);
                $customer=Customer::create([
                        'name' => $user['name'],
                        'email' => $user['email'],
                        'phone_number' => $user['phone_number'],
                        'country_id' => $user['country_id'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
    
                ]);
                $token = JWTAuth::fromUser($user);
                $msg = "User Created";
                $code = '201';
            }
            return response()->json([
                'message' => $msg,
                'access_token' => $token,
                'country' => $country,

            ], $code);
        } catch (\Exception $ex) {
            return response()->json(['status' => false, 'Messages' => $ex->getMessage()], 500);
        }
    }

    
    
}
