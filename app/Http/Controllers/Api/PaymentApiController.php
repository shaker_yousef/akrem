<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\PaymentMethod;

class PaymentApiController extends Controller
{
  public function addpaymentmethod(){
    
    $user = auth("api")->user();
    $data = [
      'profile_id' => '96282',
      'tran_type' => 'auth',
      'tran_class' => 'ecom',
      'cart_id' => 'cart_11111',
      'tokenise' => '2',
      'cart_currency' => 'AED',
      'cart_amount' => '1',
      'cart_description' => 'Description of the items/services',
      'return' => 'https://stg.api.akremapp.com/api/thankyou',
      'callback' => 'https://webhook.site/4b3af623-085f-4b82-ab22-cb6cedeba218',
      'hide_shipping' => true,
      'customer_details' => [
        'name'=> $user->name,
        'email'=> $user->email,
        'phone'=> $user->phone_number
      ],
     
    ];
    $result = Http::withHeaders([
      'authorization' => 'SHJND9KDBG-JDKKMLHZJ2-WRW9LNBLWD',
    ])
    ->post('https://secure.paytabs.com/payment/request', $data);
    if($result->successful()){
      return response()->json([
        'redirect_url' =>  $result['redirect_url']
      ]);
    }else{
      return response()->json([
        'message' =>   $result['message']
      ], 400);
     
    }
  }

  public function getpaymentmethods(){
    $user = auth("api")->user();
    $paymnetmethods = PaymentMethod::where('user_id',$user->id)->get();
    return response()->json([
      'paymnet_methods' =>  $paymnetmethods
    ]);
  }
  public function deletepaymentmethod(Request $request,$id){
    $user = auth("api")->user();
    $paymnetmethod = PaymentMethod::findOrFail($id);
    $token = $paymnetmethod->token;
    if($user->id == $paymnetmethod->user_id){
        $delete_token =[
          'profile_id' => '96282',
          'token' => $token,
        ];
        $result = Http::withHeaders([
          'authorization' => 'SHJND9KDBG-JDKKMLHZJ2-WRW9LNBLWD',
        ])
        ->post('https://secure.paytabs.com/payment/token/delete', $delete_token);
        if($result->successful()){
          $paymnetmethod->delete();
          return response()->json([
            'message' => 'Success',
          ]);
        }else{
          return response()->json([
            'message' =>   $result['message']
          ], 400);
         
        }
       
    }else{
        return response([
          'message' => 'the payment method is not for this user '
        ],400);
    }
  }

  public function payamount(Request $request,$id){
    $user = auth("api")->user();
    if($request->employee == 'E' && $request->partner == null && $request->department == null){
      $data = [
        'profile_id' => '96282',
        'tran_type' => 'sale',
        'tran_class' => 'ecom',
        'cart_id' => $id.','.floatval($request['amount']).','.'E',
        'tokenise' => '2',
        'cart_currency' => $request['currency'],
        'cart_amount' => floatval($request['amount']),
        'cart_description' => $id.','.floatval($request['amount']).','.'E',
        'return' => 'https://stg.api.akremapp.com/api/success',
        'callback' => 'https://webhook.site/4b3af623-085f-4b82-ab22-cb6cedeba218',
        'hide_shipping' => true,
        'customer_details' => [
          'name'=> $user->name,
          'email'=> $user->email,
          'phone'=> $user->phone_number
        ],
      ];
    }
    elseif($request->partner == 'P' && $request->employee == null && $request->department == null){
      $data = [
          'profile_id' => '96282',
          'tran_type' => 'sale',
          'tran_class' => 'ecom',
          'cart_id' => $id.','.floatval($request['amount']).','.'P',
          'tokenise'   => '2',
          'cart_currency' => $request['currency'],
          'cart_amount' => floatval($request['amount']),
          'cart_description' => $id.','.floatval($request['amount']).','.'P',
          'return' => 'https://stg.api.akremapp.com/api/success',
          'callback' => 'https://webhook.site/4b3af623-085f-4b82-ab22-cb6cedeba218',
          'hide_shipping' => true,
          'customer_details' => [
            'name'=> $user->name,
            'email'=> $user->email,
            'phone'=> $user->phone_number
          ],
        ];
    }
    elseif($request->partner == null && $request->employee == null && $request->department == 'D'){
      $data = [
          'profile_id' => '96282',
          'tran_type' => 'sale',
          'tran_class' => 'ecom',
          'cart_id' => $id.','.floatval($request['amount']).','.'D',
          'tokenise' => '2',
          'cart_currency' => $request['currency'],
          'cart_amount' => floatval($request['amount']),
          'cart_description' => $id.','.floatval($request['amount']).','.'D',
          'return' => 'https://stg.api.akremapp.com/api/success',
          'callback' => 'https://webhook.site/4b3af623-085f-4b82-ab22-cb6cedeba218',
          'hide_shipping' => true,
          'customer_details' => [
            'name'=> $user->name,
            'email'=> $user->email,
            'phone'=> $user->phone_number
          ],
        ];
    }
    else{
      return response([
          'message' => 'Invalid QR code'
      ],400); 
    }
    $result = Http::withHeaders([
      'authorization' => 'SHJND9KDBG-JDKKMLHZJ2-WRW9LNBLWD',
    ])
    ->post('https://secure.paytabs.com/payment/request', $data);
    if($result->successful()){
      return response()->json([
        'redirect_url' =>  $result['redirect_url']
      ]);
    }else{
      return response()->json([
        'message' =>   $result['message']
      ], 400);
     
    }
  }
}
