<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tip;
use App\Models\Profile;
use App\Models\Employee;
use App\Models\Department;
use App\Models\Country;
use App\Http\Resources\TipshistoryCollection;
use DB;
use Validator;
use Carbon\Carbon;
use App\Models\PaymentMethod;
use Illuminate\Support\Facades\Http;


class TipApiController extends Controller
{
       
        public function tipshistory($name,$order){
        $user=auth("api")->user();
        $profile=Profile::all();
           if(($name == 'all')){
               if($order == 'latest'){
                // $names = $profile->pluck('name');
                $tips = Tip::where('user_id',$user->id)
                // ->whereIn('receiver',$names)
                ->orderBy('date_time', 'desc')->paginate(10);
            //     foreach($tips as $tip){
            // }                        
            // return $tip->amount;
            //    $partner =$tip->partner;
                //    $partner =Profile::where('partner_name',$partner)->first();
                //   $country = $partner->country;
                //   $tips = collect($tip)->merge($country);
                //    $tip_collection =collect($tip);
                //    $tips = (object) array_merge((array) $country, (array) $tip);
                //   $country = Country::where('id',$partner->provider_id)->get();
                //   $tips =$tip->merge($country);
                // }
                    // $tips_partners =$tips->pluck('partner');
                    // $partner =Profile::whereIn('partner_name',$tips_partners)->get();
                    // $country =$partner->pluck('country');
                    // $tips =$tips->merge($country);
                return Response()->json([
                        'tips' => $tips,
                    ],
                );
               }
               if($order == 'newest'){
                $tips =Tip::where('user_id',$user->id)
                ->orderBy('date_time', 'asc')->paginate(10);
             return Response()->json([
                     'tips' => $tips,
                 ],
             );
            }
            if($order == 'high_price'){
                $tips =Tip::where('user_id',$user->id)
                ->orderBy('amount', 'desc')->paginate(10);
             return Response()->json([
                     'tips' => $tips,
                 ],
              
             );
            }
            if($order == 'low_price'){
                $tips =Tip::where('user_id',$user->id)
                ->orderBy('amount', 'asc')->paginate(10);
             return Response()->json([
                     'tips' => $tips,
                 ],
             
             );
            }
            }
            else{
            if($order == 'latest'){
                $partner = Profile::where('partner_name', 'LIKE', '%'. $name. '%')->first();
                if(!$partner){
                    return Response()->json([
                     'tips' => 'No Data found',
                    ],400);
                }
                $tip = Tip::where('receiver',$partner->partner_name)
                ->where('user_id',$user->id)
                ->orderBy('date_time', 'desc')->paginate(10);
                // $country =Country::where('id',$partner->country_id)->get();
                // $tip =$tip->merge($country);
                if(count($tip)){
                    return Response()->json(array('tips' => $tip));
                   }else{
                   return response()->json(['Result' => 'No Data not found'], 404);
                 }
            }
            if($order == 'newest'){
                $provider = Profile::where('partner_name', 'LIKE', '%'. $name. '%')->first();
                if(!$provider){
                    return Response()->json([
                     'tips' => 'No Data found',
                    ],400);

                }
                $tip = Tip::where('receiver',$provider->partner_name)->orderBy('date_time', 'asc')->paginate(10);
                if(count($tip)){
                    
                 return Response()->json(array('tips' => $tip));
                }
                else
                {
                return response()->json(['Result' => 'No Data not found'], 404);
              }
            }
            if($order == 'high_price'){
                $provider = Profile::where('partner_name', 'LIKE', '%'. $name. '%')->first();
                if(!$provider){
                    return Response()->json([
                     'tips' => 'No Data found',
                    ],400);

                }
                $tip = Tip::where('receiver',$provider->partner_name)->orderBy('amount', 'desc')->paginate(10);
                if(count($tip)){
                    
                 return Response()->json(array('tips' => $tip));
                }
                else
                {
                return response()->json(['Result' => 'No Data not found'], 404);
              }
            }
            if($order == 'low_price'){
                $provider = Profile::where('partner_name', 'LIKE', '%'. $name. '%')->first();
                if(!$provider){
                    return Response()->json([
                     'tips' => 'No Data found',
                    ],400);

                }
                $tip = Tip::where('receiver',$provider->partner_name)->orderBy('amount', 'asc')->paginate(10);
                if(count($tip)){
                    
                 return Response()->json(array('tips' => $tip));
                }
                else
                {
                return response()->json(['Result' => 'No Data not found'], 404);
              }
            }
        }
    
        }
        public function paytip(Request $request,$id,$payment_method_id){
            $validator = Validator::make($request->all(), [
                'amount' => 'required',
            ]);
            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }     
            $user=auth("api")->user();
            if($payment_method_id == 0){
                if($request->employee == 'E' && $request->partner == null && $request->department == null){
                    $data = [
                        'profile_id' => '96282',
                        'tran_type' => 'sale',
                        'tran_class' => 'ecom',
                        'cart_id' => $id.','.floatval($request['amount']).','.'E',
                        'tokenise' => '2',
                        'cart_currency' => $request['currency'],
                        'cart_amount' => floatval($request['amount']),
                        'cart_description' => $id.','.floatval($request['amount']).','.'E',
                        'return' => 'https://stg.api.akremapp.com/api/success',
                        'callback' => 'https://webhook.site/4b3af623-085f-4b82-ab22-cb6cedeba218',
                        'hide_shipping' => true,
                        'customer_details' => [
                          'name'=> $user->name,
                          'email'=> $user->email,
                          'phone'=> $user->phone_number
                        ],
                      ];
                }
                elseif($request->partner == 'P' && $request->employee == null && $request->department == null){
                    $data = [
                        'profile_id' => '96282',
                        'tran_type' => 'sale',
                        'tran_class' => 'ecom',
                        'cart_id' => $id.','.floatval($request['amount']).','.'P',
                        'tokenise' => '2',
                        'cart_currency' => $request['currency'],
                        'cart_amount' => floatval($request['amount']),
                        'cart_description' => $id.','.floatval($request['amount']).','.'P',
                        'return' => 'https://stg.api.akremapp.com/api/success',
                        'callback' => 'https://webhook.site/4b3af623-085f-4b82-ab22-cb6cedeba218',
                        'hide_shipping' => true,
                        'customer_details' => [
                          'name'=> $user->name,
                          'email'=> $user->email,
                          'phone'=> $user->phone_number
                        ],
                      ];
                }
                elseif($request->partner == null && $request->employee == null && $request->department == 'D'){
                    $data = [
                        'profile_id' => '96282',
                        'tran_type' => 'sale',
                        'tran_class' => 'ecom',
                        'cart_id' => $id.','.floatval($request['amount']).','.'D',
                        'tokenise' => '2',
                        'cart_currency' => $request['currency'],
                        'cart_amount' => floatval($request['amount']),
                        'cart_description' => $id.','.floatval($request['amount']).','.'D',
                        'return' => 'https://stg.api.akremapp.com/api/success',
                        'callback' => 'https://webhook.site/4b3af623-085f-4b82-ab22-cb6cedeba218',
                        'hide_shipping' => true,
                        'customer_details' => [
                          'name'=> $user->name,
                          'email'=> $user->email,
                          'phone'=> $user->phone_number
                        ],
                      ];
                }
                else{
                    return response([
                        'message' => 'Invalid QR code'
                    ],400); 
                    }
                $result = Http::withHeaders([
                  'authorization' => 'SHJND9KDBG-JDKKMLHZJ2-WRW9LNBLWD',
                ])
                ->post('https://secure.paytabs.com/payment/request', $data);
                if($result->successful()){
                  return response()->json([
                    'redirect_url' =>  $result['redirect_url']
                  ]);
                }else{
                  return response()->json([
                    'message' =>   $result['message']
                  ], 400);
                 
                }
            }else{
                $payment_method = PaymentMethod::where('id',$payment_method_id)->first();
                $recurring = [
                    'profile_id' => '96282',
                    'tran_type' =>  'sale',
                    'tran_class' =>  'recurring',
                    'tran_ref' =>  $payment_method->tran_ref,
                    'token' =>   $payment_method->token,
                    'cart_id' =>  'cart_11111',
                    'cart_currency' =>  $request['currency'],
                    'cart_amount' =>  floatval($request['amount']),
                    'cart_description' =>  'Description of the items/services',
                    'callback' =>  'https://webhook.site/4b3af623-085f-4b82-ab22-cb6cedeba218',
                ];
                $recurring_call = Http::withHeaders([
                'authorization' => 'SHJND9KDBG-JDKKMLHZJ2-WRW9LNBLWD',
                ])
                ->post('https://secure.paytabs.com/payment/request', $recurring);
                if(!$recurring_call->successful()){
                    return response()->json([
                        'message' =>   $recurring_call['message']
                      ], 400);
                } 
                $payment_method_ref=$payment_method->update([
                'tran_ref' => $recurring_call['tran_ref'],
                ]);

                if($request->employee == 'E' && $request->partner == null && $request->department == null){
                    $employee = Employee::find($id);
                    if(!$employee){
                        return response([
                            'message' => 'Invalid QR code'
                        ],400);
                    }else{
                        $partner=Profile::where('id',$employee->provider_id)->first();
                       $tip_currency = $partner->country->currency;
                        $tip =[
                            'amount' => floatval($request['amount']),
                            'tipper' =>  $user->name,
                            // 'receiver' => $employee->full_name,
                            'receiver' => $partner->partner_name,
                            'date_time' => Carbon::now(),
                            
                        ];    
                        $data=Tip::create([
                            'amount' => $tip['amount'],
                            'payment_method' => 'Master Card',
                            'transaction_id' => '',
                            'user_id' => $user->id,
                            'partner_id' =>  $partner->id,
                            'department_id' =>  $employee->department_id,
                            'employee_id' =>  $employee->id,
                            'partner' =>  $partner->partner_name,
                            'department' =>  $employee->department->name,
                            'employee' =>  $employee->full_name,
                            'receiver' => $tip['receiver'],
                            'currency' => $tip_currency,
                            'date_time' => Carbon::now(),
                        ]);
                        return response()->json($tip);
                    }
                    
                }
                elseif($request->partner == 'P' && $request->employee == null && $request->department == null){
                    $partner = Profile::find($id);
                    $tip_currency = $partner->country->currency;
                    if(!$partner){
                        return response([
                            'message' => 'Invalid QR code'
                        ],400);
                    }else{
                        $tip =[
                            'amount' => floatval($request['amount']),
                            'tipper' => $user->name,
                            'receiver' => $partner->partner_name,
                            'date_time' => Carbon::now(),
                        ];
                        $data=Tip::create([
                            'amount' => $tip['amount'],
                            'payment_method' => 'Master Card',
                            'transaction_id' => '',
                            'user_id' => $user->id,
                            'partner_id' => $partner->id,
                            'partner' => $partner->partner_name,
                            'receiver' => $tip['receiver'],
                            'currency' => $tip_currency,
                            'date_time' => Carbon::now(),
                        ]);
                        return response()->json($tip);
                        }
                }
                elseif($request->partner == null && $request->employee == null && $request->department == 'D'){
                    $department = Department::find($id);
                    if(!$department){
                        return response([
                            'message' => 'Invalid QR code'
                        ],400);
                    }else{
                        $partner =Profile::where('id',$department->partner_id)->first();
                        $tip_currency = $partner->country->currency;
                        $tip =[
                            'amount' => floatval($request['amount']),
                            'tipper' => $user->name,
                            'receiver' => $partner->partner_name,
                            'date_time' => Carbon::now(),
                        ];
                        $data=Tip::create([
                            'amount' => $tip['amount'],
                            'payment_method' => 'Master Card',
                            'transaction_id' => '',
                            'user_id' => $user->id,
                            'department_id' => $department->id,
                            'partner_id' => $partner->id,
                            'department' => $department->name,
                            'partner' => $partner->partner_name,
                            'receiver' => $tip['receiver'],
                            'currency' => $tip_currency,
                            'date_time' => Carbon::now(),
                        ]);
                        return response()->json($tip);
                        }
                }
                else{
                    return response([
                        'message' => 'Invalid QR code'
                    ],400); 
                    }
               
            }
            
           
        }
}
