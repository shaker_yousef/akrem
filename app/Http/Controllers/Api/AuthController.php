<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Customer;
use App\Models\Country;
use App\Models\Employee;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use DB;
use Carbon\Carbon;
use Validator;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['loginemail','loginphone', 'register','registercheck']]);
    }
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginemail(Request $request){
        $email= $request->email ;
        $user=User::where('email',$email)->first();
        if(!$user){
            return response()->json([
                'message' => 'ًThere is no account that matches what you entered',
            ], 400);
        }
        if($user->role_id==1 ||$user->role_id==3 ){
            return response()->json([
                        'message' => 'You can not login',
                    ], 400);
        }
        if($user->role_id==2 ||$user->role_id==4 ){
            if($user->role_id==2){
                $customer = Customer::where('email',$user->email)->first();
                if($customer->is_active == 0){
                    return response()->json([
                        'message' => 'You can not login',
                    ], 400); 
                }
            }else{
                $employee = Employee::where('email',$user->email)->first();
                if($employee->is_active == 0){
                    return response()->json([
                        'message' => 'You can not login',
                    ], 400); 
                }
            }
            
        }
    	$validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        if (! $token = auth("api")->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->createNewToken($token);
    }
    public function loginphone(Request $request){
        $phone_number= $request->phone_number ;
        $user=User::where('phone_number',$phone_number)->first();
        if(!$user){
            return response()->json([
                'message' => 'ًThere is no account that matches what you entered',
            ], 400);
        }
        if($user->role_id==1 ||$user->role_id==3 ){
            return response()->json([
                        'message' => 'You can not login',
                    ], 400);
        }
        if($user->role_id==2 ||$user->role_id==4 ){
            if($user->role_id==2){
                $customer = Customer::where('email',$user->email)->first();
                if($customer->is_active == 0){
                    return response()->json([
                        'message' => 'You can not login',
                    ], 400); 
                }
            }else{
                $employee = Employee::where('email',$user->email)->first();
                if($employee->is_active == 0){
                    return response()->json([
                        'message' => 'You can not login',
                    ], 400); 
                }
            }
            
        }
    	$validator = Validator::make($request->all(), [
            'phone_number' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        if (! $token = auth("api")->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->createNewToken($token);
    }
    
    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => ['required', 'unique:users,email', 'email'],
            'password' => 'required',
            'phone_number' => ['required', 'unique:users,phone_number'],
            'confirm_password' => 'required|same:password',
            'country_id' => 'required',

        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $user = User::create(array_merge(
                    $validator->validated(),
                    ['password' => bcrypt($request->password)],
                    ['email_verified_at' => Carbon::now()],

                ));
               
            $customer=Customer::create([
                    'name' => $user['name'],
                    'email' => $user['email'],
                    'phone_number' => $user['phone_number'],
                    'country_id' => $user['country_id'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),

                ]);
        $token = JWTAuth::fromUser($user);
        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user ,
            'token' => $token,

        ], 201);
    }
    public function registercheck(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'unique:users,email', 'email'],
            'phone_number' => ['required', 'unique:users,phone_number'],
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
       
        return response()->json([
       ], 200);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        auth("api")->logout();
        return response()->json(['message' => 'User successfully signed out']);
    }
    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return $this->createNewToken(auth("api")->refresh());
    }
    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile() {
        return response()->json(auth("api")->user());
    }
    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
        $user =auth("api")->user();
        $country= Country::where('id',$user->country_id)->first();
        if(!$country){
            return response()->json([
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth("api")->factory()->getTTL() * 6000,
                'user' => auth("api")->user(),
            ]);
        }else{
            return response()->json([
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth("api")->factory()->getTTL() * 6000,
                'user' => auth("api")->user(),
                'country' => $country,
            ]);
        }
       
    }
    
}