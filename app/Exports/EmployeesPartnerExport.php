<?php

namespace App\Exports;

use App\Models\Employee;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EmployeesPartnerExport implements FromCollection, WithHeadings, WithMapping
{
    protected $partner;

    public function __construct($partner)
    {
        $this->partner = $partner;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Employee::where('provider_id',$this->partner)->get();
    }

    public function headings(): array

    {

        return [

            'Full Name',
            'Work Title',
            'Department ID',
            'ID Number',
            'Bank Details',
            'Bank Name',
            'Transfer Customer Name',
            'AC',
            'IBAN',
            'Swift Code',
            'Mobile Number',
            'Email',
            'Credit Frequency',
            'Provider ID',
            'Is Active',
            'Created At',
            'Updated At',

        ];

    }
    public function map($row): array{
        $fields = [
           $row->full_name,
           $row->work_title,
           $row->department_id,
           $row->id_number,
           $row->bank_details,
           $row->bank_name,
           $row->transfer_customer_name,
           $row->ac,
           $row->iban,
           $row->swift_code,
           $row->mobile_number,
           $row->email,
           $row->credit_frequency,
           $row->provider_id,
           $row->is_active,
           $row->created_at,
           $row->updated_at,
          
      ];
     return $fields;
 }
}
