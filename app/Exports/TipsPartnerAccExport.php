<?php

namespace App\Exports;

use App\Models\Tip;
use App\Models\User;
use App\Models\Profile;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class TipsPartnerAccExport implements FromCollection, WithHeadings, WithMapping
{
    protected $value;

    public function __construct($provider_fixed,$fees_partner,$fees_country)
    {
        $this->provider_fixed = $provider_fixed;
        $this->fees_partner = $fees_partner;
        $this->fees_country = $fees_country;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $provider=auth()->user()->id;
        $provider=User::findOrFail($provider);
        $provider=Profile::where('contact_email',$provider->email)->first();
        $tips = Tip::where('receiver',$provider->partner_name)->get();
     
        return $tips;
    }
    public function headings(): array

    {

        return [

            'Amount',
            'Department',
            'Date Time',
        ];

    }
    public function map($row): array{
        if($this->provider_fixed == 1){
            $fields = [
                $row->amount -$this->fees_partner - $this->fees_country,
                $row->department,
                $row->date_time,
               
           ];
         }else{
            $fields = [
                $row->amount - ($this->fees_partner/100) *($row->amount - $this->fees_country)- $this->fees_country,
                $row->department,
                $row->date_time,
               
           ];
         }
     return $fields;
 }
}
