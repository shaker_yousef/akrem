<?php

namespace App\Imports;

use App\Models\Employee;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class EmployeesImport implements ToModel,WithHeadingRow
{
    protected $partner;

    public function __construct($partner)
    {
        $this->partner = $partner;
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $user=User::create([
            'name' => $row['full_name'],
            'email' => $row['email'],
            'email_verified_at' => Carbon::now(),
            'phone_number' => $row['mobile_number'],
            'password' => $row['password'],
            'role_id' =>4,
            'image' => $row['profile_image'],

            
        ]);
        $employee = Employee::create([
            'full_name'  => $row['full_name'],
            'work_title'=> $row['work_title'],
            'department_id'=> $row['department'],
            'id_number'=> $row['id_number'],
            'id_front_pic'=> $row['id_front_pic'],
            'id_back_pic'=> $row['id_back_pic'],
            'bank_details'=> $row['bank_details'],
            'bank_name'=> $row['bank_name'],
            'transfer_customer_name'=> $row['transfer_customer_name'],
            'ac'=> $row['ac'],
            'iban'=>$row['iban'],
            'swift_code'=> $row['swift_code'],
            'mobile_number'=> $row['mobile_number'],
            'email'=> $row['email'],
            'password'=>\Hash::make($row['password']),
            'credit_frequency'=> $row['credit_frequency'],
            'profile_image'=> $row['profile_image'],
            'provider_id'=> $this->partner,
           
        ]);
        $qr_name=$employee->full_name."_". Str::random(6).".png";
        $qr_code=QrCode::format('png')
        ->merge('/public/assets/img/logo.png', .3, false)
        ->size(512)
        ->errorCorrection('H')->generate('E'.$employee->id, public_path('storage/images/'.$qr_name));
        $employee->update([
            'qr_code' =>  $qr_name,
        ]);
        // $employee =new Employee([
        //     'full_name'  => $row['full_name'],
        //     'work_title'=> $row['work_title'],
        //     'department_id'=> $row['department'],
        //     'id_number'=> $row['id_number'],
        //     'id_front_pic'=> $row['id_front_pic'],
        //     'id_back_pic'=> $row['id_back_pic'],
        //     'bank_details'=> $row['bank_details'],
        //     'bank_name'=> $row['bank_name'],
        //     'transfer_customer_name'=> $row['transfer_customer_name'],
        //     'ac'=> $row['ac'],
        //     'iban'=>$row['iban'],
        //     'swift_code'=> $row['swift_code'],
        //     'mobile_number'=> $row['mobile_number'],
        //     'email'=> $row['email'],
        //     'password'=>\Hash::make($row['password']),
        //     'credit_frequency'=> $row['credit_frequency'],
        //     'profile_image'=> $row['profile_image'],
        //     'provider_id'=> $this->partner,
        //     'qr_code' =>QrCode::format('png')->size(512)
        //     ->errorCorrection('H')
        //     ->generate('E'.$row['full_name'], public_path('storage/images/'.$row['full_name']."_". Str::random(6).".png"))
        // ]);
       
        // dd($employee);
        return $employee;
        
       
      
    }
}
