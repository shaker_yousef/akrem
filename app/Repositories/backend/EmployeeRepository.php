<?php

namespace App\Repositories\backend;

use App\Repositories\BaseRepository;
use App\Models\Employee;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Events\Backend\Employee\EmployeeCreated;
use App\Events\Backend\Employee\EmployeeUpdated;
use App\Exceptions\GeneralException;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;


/**
 * Class EmployeeRepository.
 */
class EmployeeRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Employee::class;
    }
    public function create(array $data): Employee
    {
        // Make sure it doesn't already exist
        if ($this->employeeExists($data['full_name'])) {
            throw new GeneralException('An employee already exists with the name '.e($data['full_name']));
        }
        return DB::transaction(function () use ($data) {
            $employee = parent::create([
                'full_name' => $data['full_name'],
                'work_title' => $data['work_title'],
                'department_id' => $data['department_id'],
                'id_number' => $data['id_number'],
                'id_front_pic' => $this->UploadFile($data['id_front_pic'],'images/employees/id_front_pic'),
                'id_back_pic' => $this->UploadFile($data['id_back_pic'],'images/employees/id_back_pic'),
                'bank_details' => $data['bank_details'],
                'bank_name' => $data['bank_name'],
                'transfer_customer_name' => $data['transfer_customer_name'],
                'ac' => $data['ac'],
                'iban' => $data['iban'],
                'swift_code' => $data['swift_code'],
                'mobile_number' => $data['mobile_number'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'credit_frequency' => $data['credit_frequency'],
                'profile_image' => $this->UploadFile($data['profile_image'],'images/employees/profile_image'),
                'provider_id' =>$data['partner'],
            ]);

            $employee->user()->create([
                'name' => $employee['full_name'],
                'email' => $employee['email'],
                'email_verified_at' => Carbon::now(),
                'phone_number' => $employee['mobile_number'],
                'password' => $employee['password'],
                'role_id' =>4,
                'image' => $employee['profile_image'],


            ]);
            $qr_name=$employee->name."_". Str::random(6).".png";
            $qr_code=QrCode::format('png')
            ->merge('/public/assets/img/logo.png', .3, false)
            ->size(512)
            ->errorCorrection('H')->generate('E'.$employee->id, public_path('storage/images/'.$qr_name));
            $employee->update([
                'qr_code' =>  $qr_name,
            ]);
            if ($employee) {
                event(new EmployeeCreated($employee));
                return $employee;
            }
            throw new GeneralException(trans('exceptions.backend.access.employees.create_error'));

        });
    }
    public function update(Employee $employee, array $data): Employee
    {
        if (strtolower($employee->full_name) !== strtolower($data['full_name'])) { 
            if ($this->employeeExists($data['full_name'])) {
                throw new GeneralException('An employee already exists with the name '.$data['full_name']);
            }
        }

        return DB::transaction(function () use ($employee, $data) {
            if (isset($data['id_front_pic'])) {
                $employee->update([
                    'id_front_pic' => $this->UploadFile($data['id_front_pic'], 'images/employees/id_front_pic')
                ]);
            } else {
                $employee->update([
                    'id_front_pic' => $employee->id_front_pic
                ]);
            }
            if (isset($data['id_back_pic'])) {
                $employee->update([
                    'id_back_pic' => $this->UploadFile($data['id_back_pic'], 'images/employees/id_back_pic')
                ]);
            } else {
                $employee->update([
                    'id_back_pic' => $employee->id_back_pic
                ]);
            }
            if (isset($data['profile_image'])) {
                $employee->update([
                    'profile_image' => $this->UploadFile($data['profile_image'], 'images/employees/profile_image')
                ]);
            } else {
                $employee->update([
                    'profile_image' => $employee->profile_image
                ]);
            }
            if ($employee->update([
                'full_name' => $data['full_name'],
                'work_title' => $data['work_title'],
                'department_id' => $data['department_id'],
                'id_number' => $data['id_number'],
                'bank_details' => $data['bank_details'],
                'bank_name' => $data['bank_name'],
                'transfer_customer_name' => $data['transfer_customer_name'],
                'ac' => $data['ac'],
                'iban' => $data['iban'],
                'swift_code' => $data['swift_code'],
                'mobile_number' => $data['mobile_number'],
                'email' => $data['email'],
                'credit_frequency' => $data['credit_frequency'],
                //'provider_id' =>$data['partner'],

            ])

            ) {
                $user = User::where('email',$data['email'])->first();
                $user->update([
                    'name' => $employee['full_name'],
                    'email' => $employee['email'],
                    'email_verified_at' => Carbon::now(),
                    'phone_number' => $employee['mobile_number'],
                    'password' => $employee['password'],
                    'role_id' =>4,
                    'image' => $employee['profile_image'],
                ]);
                return $employee;
            }
            if ($employee) {
                event(new EmployeeUpdated($employee));
                return $employee;
            }
            throw new GeneralException(trans('exceptions.backend.access.employees.update_error'));

        });
    }




    protected function employeeExists($name): bool
    {
        return $this->model
                ->where('full_name', strtolower($name))
                ->count() > 0;
    }

    // Upload image function
    public function UploadFile($file, $name)
    {
        //dd($file);
        //get file name with extention
        $filenameWithExt = $file->getClientOriginalName();
        //get just file name
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        //GET EXTENTION
        $extention = $file->getClientOriginalExtension();
        //file name to store
        $fileNameToStore = $filename . '_' . time() . '.' . $extention;
        //upload image
        $path = $file->storeAs('public/' . $name . '/', $fileNameToStore);
        return $fileNameToStore;
    }


    public function updateprofileimage(Employee $employee, array $data): Employee
    {
      
        return DB::transaction(function () use ($employee, $data) {
           
            if (isset($data['profile_image'])) {
                $employee->update([
                    'profile_image' => $this->UploadFile($data['profile_image'], 'images/employees/profile_image')
                ]);
            } else {
                $employee->update([
                    'profile_image' => $employee->profile_image
                ]);
            }
           
            if ($employee) {
                event(new EmployeeUpdated($employee));
                return $employee;
            }
            throw new GeneralException(trans('exceptions.backend.access.employees.update_error'));

        });
    }
}
