<?php

namespace App\Repositories\backend;
use App\Repositories\BaseRepository;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Events\Backend\Customer\CustomerUpdated;
use App\Events\Backend\Customer\CustomerCreated;
use App\Events\Backend\Customer\CustomerDeactivated;
use App\Events\Backend\Customer\CustomerActivated;
use App\Exceptions\GeneralException;


class CustomerRepository extends BaseRepository
{
   /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Customer::class;
    }
    public function create(array $data): Customer
    {
        // Make sure it doesn't already exist
        // if ($this->customerExists($data['name'])) {
        //     throw new GeneralException('An customer already exists with the name '.e($data['name']));
        // }
        return DB::transaction(function () use ($data) {
            $customer = parent::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'phone_number' => $data['phone_number'],
                'country' => $data['country'],
            ]);
            if ($customer) {
                event(new CustomerCreated($customer));
                return $customer;
            }
            // throw new GeneralException(trans('exceptions.backend.access.customers.create_error'));

        });
    }
    public function update(Customer $customer, array $data): Customer
    {
        
        if (strtolower($customer->name) !== strtolower($data['name'])) {
            if ($this->customerExists($data['name'])) {
                throw new GeneralException('An customer already exists with the name '.$data['name']);
            }
        }
        return DB::transaction(function () use ($customer, $data) {
         
            if ($customer->update([
                'name' => $data['name'],
                'email' => $data['email'],
                'phone_number' => $data['phone_number'],
                'country_id' => $data['country_id'],
            ])) {
                $user = User::where('email',$data['email'])->first();
                $user->update([
                    'name' => $customer['name'],
                    'email' => $customer['email'],
                    'phone_number' => $customer['phone_number'],
                    'country_id' => $customer['country_id'],
                ]);
                return $customer;
            }
            if ($customer) {
                event(new CustomerUpdated($customer));
                return $customer;
            }
            throw new GeneralException(trans('exceptions.backend.access.customers.update_error'));

        });
    }
    public function active(Customer $customer): Customer
    {
        if ($customer->is_active) {
            throw new GeneralException(__('exceptions.backend.customer.already_activated'));
        }
        $customer=Customer::create([
            'name' => $customer['name'],
            'email' => $customer['email'],

        ]);
        $customer->is_active = 1;
        // $customer->profile_id=$profile->id;
        $activated = $customer->save();
        if ($activated) {
            event(new CustomerActivated($activated));

//            // Let user know their account was approved
//            if (config('access.users.requires_approval')) {
//                $user->notify(new UserAccountActive);
//            }

            return $customer;
        }

        throw new GeneralException(__('exceptions.backend.customer.cant_active'));
    }

    public function deactive(Customer $customer): Customer
    {
        if ($customer->is_active) {
            throw new GeneralException(__('exceptions.backend.customer.already_deactivated'));
        }
        $customer=Customer::create([
            'name' => $data['name'],
            'email' => $data['email'],
           
        ]);
        $customer->is_active = 0;
        // $customer->provider_id=$provider->id;
        $deactivated = $customer->save();
        if ($deactivated) {
            event(new CustomerDeactivated($deactivated));

//            // Let user know their account was approved
//            if (config('access.users.requires_approval')) {
//                $user->notify(new UserAccountActive);
//            }

            return $customer;
        }

        throw new GeneralException(__('exceptions.backend.customer.cant_deactive'));
    }

    protected function customerExists($name): bool
    {
        return $this->model
                ->where('name', strtolower($name))
                ->count() > 0;
    }
}
