<?php


namespace App\Repositories\backend;


use App\Events\Backend\Tip\TipCreated;
use App\Events\Backend\Tip\TipUpdated;
use App\Exceptions\GeneralException;
use App\Models\Tip;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Testing\Fluent\Concerns\Has;
use Carbon\Carbon;

class TipRepository extends BaseRepository
{
    public function model()
    {
        return Tip::class;
    }

    public function create(array $data): Tip
    { 
        return DB::transaction(function () use ($data) {
            $tip = parent::create([
                'amount' => $data['amount'],
                'payment_method' => $data['payment_method'],
                'transaction_id' => $data['transaction_id'],
                'user_id' => auth()->user()->id,
                'receiver' => $data['receiver'],
                'date_time' => Carbon::now(),
                
            ]);
            if ($tip) {
                event(new TipCreated($tip));
                return $tip;
            }
            throw new GeneralException(trans('exceptions.backend.access.tips.create_error'));

        });
    }

    public function update(Tip $tip, array $data): Tip
    {
        return DB::transaction(function () use ($tip, $data) {
         
            if ($tip->update([
                'amount' => $data['amount'],
                'payment_method' => $data['payment_method'],
                'transaction_id' => $data['transaction_id'],
                'user_id' => $data['tipper'],
                'receiver' => $data['receiver'],
                'date_time' => Carbon::now(),

                
            ])) {
                return $tip;
            }
            if ($tip) {
                event(new TipUpdated($tip));
                return $tip;
            }
            throw new GeneralException(trans('exceptions.backend.access.tips.update_error'));

        });
    }

   

}
