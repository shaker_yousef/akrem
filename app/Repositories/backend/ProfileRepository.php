<?php

namespace App\Repositories\backend;
use App\Repositories\BaseRepository;
use App\Models\Profile;
use App\Events\Backend\Profile\ProfileUpdated;
use App\Events\Backend\Profile\ProfileCreated;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Events\Backend\Profile\ProfileActivated;
use App\Events\Backend\Profile\ProfileDeactivated;
use App\Events\Backend\Profile\ProfileConfirmed;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Models\User;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Str;

class ProfileRepository extends BaseRepository
{
   /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Profile::class;
    }
    public function create(array $data): Profile
    {
        // Make sure it doesn't already exist
        if ($this->profileExists($data['partner_name'])) {
            throw new GeneralException('A partner already exists with the name '.e($data['partner_name']));
        }
        return DB::transaction(function () use ($data) {
                $profile = parent::create([
                'partner_name' => $data['partner_name'],
                'bank_details' => $data['bank_details'],
                'bank_name' => $data['bank_name'],
                'trn' => $data['trn'],
                'transfer_customer_name' => $data['transfer_customer_name'],
                'ac' => $data['ac'],
                'iban' => $data['iban'],
                'swift_code' => $data['swift_code'],
                'company_registration_document' => $this->UploadFile($data['company_registration_document'], 'files/company_registration_document'),
                'phone_number' => $data['phone_number'],
                'mobile_number' => $data['mobile_number'],
                'contact_email' => $data['contact_email'],
                'password' => Hash::make($data['password']),
                'website' => $data['website'],
                'address' => $data['address'],
                'logo' => $this->UploadFile($data['logo'], 'images/partners'),
                'services' => $data['services'],
                'credit_method' => $data['credit_method'],
                'country_id' => $data['country_id'],
                'special_fees' => $data['special_fees'],

            ]);
            $qr_name=$profile->name."_". Str::random(6).".png";
            $qr_code=QrCode::format('png')
            ->merge('/public/assets/img/logo.png', .3, false)
            ->size(512)->errorCorrection('H')->generate('P'.$profile->id, public_path('storage/images/'.$qr_name));
            $profile->update([
                'qr_code_provider' =>  $qr_name,

            ]);
            if($data['statusfilter'] == 2){
                $profile->update([
                    'fees' =>  $data['fixed_amount'],
                    'is_fixed' => 1
    
                ]);
            }else{
                $profile->update([
                    'fees' =>  $data['percentage'],
    
                ]);
            }
            $profile->user()->create([
                'name' => $profile['partner_name'],
                'email' => $profile['contact_email'],
                'email_verified_at' => Carbon::now(),
                'password' => $profile['password'],
                'role_id' =>3,
                'country_id' => $data['country_id'],

            ]);

            if ($profile) {
                event(new ProfileCreated($profile));
                return $profile;
            }
            throw new GeneralException(trans('exceptions.backend.access.partners.create_error'));

        });
    }
    public function update(Profile $profile, array $data): Profile
    {
        if (strtolower($profile->partner_name) !== strtolower($data['partner_name'])) { 
            if ($this->profileExists($data['partner_name'])) {
                throw new GeneralException('A partner already exists with the name '.$data['partner_name']);
            }
        }
      
        return DB::transaction(function () use ($profile, $data) {
            if (isset($data['logo'])) {
                $profile->update([
                    'logo' => $this->UploadFile($data['logo'], 'images/partners')
                ]);
            } else {
                $profile->update([
                    'logo' => $profile->logo
                ]);
            }
            if (isset($data['company_registration_document'])) {
                $profile->update([
                    'company_registration_document' => $this->UploadFile($data['logo'], 'files/company_registration_document')
                ]);
            } else {
                $profile->update([
                    'company_registration_document' => $profile->company_registration_document
                ]);
            }
            if (isset($data['statusfilter'])) {
                if($data['statusfilter'] == 2){
                    $profile->update([
                        'fees' =>  $data['fixed_amount'],
                        'is_fixed' => 1
        
                    ]);
                }else{
                    $profile->update([
                        'fees' =>  $data['percentage'],
        
                    ]);
                }
            } else {
                $profile->update([
                    'fees' => $profile->fees
                ]);
            }
            if ($profile->update([
                'partner_name' => $data['partner_name'],
                'bank_details' => $data['bank_details'],
                'bank_name' => $data['bank_name'],
                'trn' => $data['trn'],
                'transfer_customer_name' => $data['transfer_customer_name'],
                'ac' => $data['ac'],
                'iban' => $data['iban'],
                'swift_code' => $data['swift_code'],
                'phone_number' => $data['phone_number'],
                'mobile_number' => $data['mobile_number'],
                'contact_email' => $data['contact_email'],
                'website' => $data['website'],
                'address' => $data['address'],
                'services' => $data['services'],
                'credit_method' => $data['credit_method'],
                'country_id' => $data['country_id'],
                'special_fees' => $data['special_fees'],

            ]))
           

             {
                $provider=User::where('email',$profile->contact_email)->first();
                $provider->update([
                'name' => $data['partner_name'],
                'email' => $data['contact_email'],
                'email_verified_at' => Carbon::now(),
                'role_id' =>3,
                'country_id' => $data['country_id'],
                ]);
                 return $profile;
            }

            if ($profile) {
                event(new ProfileUpdated($profile));
                return $profile;
            }
            throw new GeneralException(trans('exceptions.backend.access.partners.update_error'));

        });
    }

    public function active(Profile $profile): Profile
    {
        if ($profile->status) {
            throw new GeneralException(__('exceptions.backend.profile.already_activated'));
        }
        $profile=Profile::create([
            'partner_name' => $profile['partner_name'],
            'bank_details' => $profile['bank_details'],
            'bank_name' => $profile['bank_name'],
            'trn' => $profile['trn'],
            'transfer_customer_name' => $profile['transfer_customer_name'],
            'ac' => $profile['ac'],
            'iban' => $profile['iban'],
            'swift_code' => $profile['swift_code'],
            'company_registration_document' => $profile['company_registration_document'],
            'phone_number' => $profile['phone_number'],
            'mobile_number' => $profile['mobile_number'],
            'contact_email' => $profile['contact_email'],
            'password' => Hash::make($profile['password']),
            'website' => $profile['website'],
            'address' => $profile['address'],
            'logo' => $profile['logo'],
            'services' => $profile['services'],
            'credit_method' => $profile['credit_method'],
            'status' => $profile['status'],
            'confirmation_status' => $profile['confirmation_status'],

        ]);
        $profile->status = 1;
        $profile->profile_id=$profile->id;
        $activated = $profile->save();
        if ($activated) {
            event(new ProfileActivated($activated));

//            // Let user know their account was approved
//            if (config('access.users.requires_approval')) {
//                $user->notify(new UserAccountActive);
//            }

            return $profile;
        }

        throw new GeneralException(__('exceptions.backend.profile.cant_active'));
    }
    public function confirm(Profile $profile): Profile
    {
        if ($profile->confirmation_status) {
            throw new GeneralException(__('exceptions.backend.profile.confirmed'));
        }
        $profile=Profile::create([
            'partner_name' => $profile['partner_name'],
            'bank_details' => $profile['bank_details'],
            'bank_name' => $profile['bank_name'],
            'trn' => $profile['trn'],
            'transfer_customer_name' => $profile['transfer_customer_name'],
            'ac' => $profile['ac'],
            'iban' => $profile['iban'],
            'swift_code' => $profile['swift_code'],
            'company_registration_document' => $profile['company_registration_document'],
            'phone_number' => $profile['phone_number'],
            'mobile_number' => $profile['mobile_number'],
            'contact_email' => $profile['contact_email'],
            'password' => Hash::make($profile['password']),
            'website' => $profile['website'],
            'address' => $profile['address'],
            'logo' => $profile['logo'],
            'services' => $profile['services'],
            'credit_method' => $profile['credit_method'],
            'status' => $profile['status'],
            'confirmation_status' => $profile['confirmation_status'],

        ]);
        $profile->confirmation_status = 1;
        $profile->profile_id=$profile->id;
        $confirmed = $profile->save();
        if ($confirmed) {
            event(new ProfileActivated($confirmed));

//            // Let user know their account was approved
//            if (config('access.users.requires_approval')) {
//                $user->notify(new UserAccountActive);
//            }

            return $profile;
        }

        throw new GeneralException(__('exceptions.backend.profile.cant_confirm'));
    }

    public function deactive(Profile $provider): Profile
    {
        if ($provider->status) {
            throw new GeneralException(__('exceptions.backend.profile.already_deactivated'));
        }
        $provider=Profile::create([
            'partner_name' => $provider['partner_name'],
            'bank_details' => $profile['bank_details'],
            'bank_name' => $provider['bank_name'],
            'trn' => $provider['trn'],
            'transfer_customer_name' => $provider['transfer_customer_name'],
            'ac' => $provider['ac'],
            'iban' => $provider['iban'],
            'swift_code' => $provider['swift_code'],
            'company_registration_document' => $provider['company_registration_document'],
            'phone_number' => $provider['phone_number'],
            'mobile_number' => $provider['mobile_number'],
            'contact_email' => $provider['contact_email'],
            'password' => Hash::make($provider['password']),
            'website' => $provider['website'],
            'address' => $provider['address'],
            'logo' => $provider['logo'],
            'services' => $provider['services'],
            'credit_method' => $provider['credit_method'],
            'status' => $provider['status'],
            'confirmation_status' => $provider['confirmation_status'],
        ]);
        $provider->status = 0;
        $provider->provider_id=$provider->id;
        $deactivated = $provider->save();
        if ($deactivated) {
            event(new ProfileDeactivated($deactivated));

//            // Let user know their account was approved
//            if (config('access.users.requires_approval')) {
//                $user->notify(new UserAccountActive);
//            }

            return $provider;
        }

        throw new GeneralException(__('exceptions.backend.profile.cant_deactive'));
    }

    protected function profileExists($name): bool
    {
        return $this->model
                ->where('partner_name', strtolower($name))
                ->count() > 0;
    }

    // Upload image function
    public function UploadFile($file, $name)
    {
        //get file name with extention
        $filenameWithExt = $file->getClientOriginalName();
        //get just file name
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        //GET EXTENTION
        $extention = $file->getClientOriginalExtension();
        //file name to store
        $fileNameToStore = $filename . '_' . time() . '.' . $extention;
        //upload image
        $path = $file->storeAs('public/' . $name . '/', $fileNameToStore);
        return $fileNameToStore;
    }
}
