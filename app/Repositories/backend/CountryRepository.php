<?php

namespace App\Repositories\backend;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Events\Backend\Country\CountryCreated;
use App\Events\Backend\Country\CountryUpdated;
use Illuminate\Support\Str;
use App\Repositories\BaseRepository;
use App\Models\Country;

class CountryRepository extends BaseRepository
{
     /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Country::class;
    }
    public function create(array $data): Country
    {
        // Make sure it doesn't already exist
        if ($this->countryExists($data['name'])) {
            throw new GeneralException('A country already exists with the name '.e($data['name']));
        }
        return DB::transaction(function () use ($data) {
                $country = parent::create([
                'name' => $data['name'],
                'flag' => $this->UploadFile($data['flag'],'images/countries/flag'),
                'currency' => $data['currency'],
                'fees' => $data['fees'],
            ]);
            
            if ($country) {
                event(new CountryCreated($country));
                return $country;
            }
            throw new GeneralException(trans('exceptions.backend.access.countries.create_error'));

        });
    }
    public function update(Country $country, array $data): Country
    {
        if (strtolower($country->name) !== strtolower($data['name'])) { 
            if ($this->countryExists($data['name'])) {
                throw new GeneralException('An country already exists with the name '.$data['name']);
            }
        }
      
        return DB::transaction(function () use ($country, $data) {
            if (isset($data['flag'])) {
                $country->update([
                    'flag' => $this->UploadFile($data['flag'], 'images/countries/flag')
                ]);
            } else {
                $country->update([
                    'flag' => $country->flag
                ]);
            }
            
            if ($country->update([
                'name' => $data['name'],
                'currency' => $data['currency'],
                'fees' => $data['fees'],
            ]))
             {
                return $country;
            }

            if ($country) {
                event(new CountryUpdated($country));
                return $country;
            }
            throw new GeneralException(trans('exceptions.backend.access.countries.update_error'));

        });
    }

    
    

    

    protected function countryExists($name): bool
    {
        return $this->model
                ->where('name', strtolower($name))
                ->count() > 0;
    }

    // Upload image function
    public function UploadFile($file, $name)
    {
       
        //get file name with extention
        $filenameWithExt = $file->getClientOriginalName();
        //get just file name
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        //GET EXTENTION
        $extention = $file->getClientOriginalExtension();
        //file name to store
        $fileNameToStore = $filename . '_' . time() . '.' . $extention;
        //upload image
        $path = $file->storeAs('public/' . $name . '/', $fileNameToStore);
        return $fileNameToStore;
    }
}
