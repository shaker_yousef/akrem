<?php

namespace App\Repositories\backend;
use App\Repositories\BaseRepository;
use App\Models\Department;
use Illuminate\Support\Facades\DB;
use App\Events\Backend\Department\DepartmentUpdated;
use App\Events\Backend\Department\DepartmentCreated;
use App\Exceptions\GeneralException;
use Illuminate\Support\Str;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\Models\User;
use App\Models\Profile;



class DepartmentRepository extends BaseRepository
{
   /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Department::class;
    }
    public function create(array $data): Department
    { 
        //Make sure it doesn't already exist
        if ($this->departmentExists($data['name'])) {
            throw new GeneralException('A department already exists with the name '.e($data['name']));
        }
        return DB::transaction(function () use ($data) {
            $user=auth()->user()->id;
            $user=User::findOrFail($user);
            $partner=Profile::where('contact_email',$user->email)->first();
            $partner =$partner->id;
            $department = parent::create([
                'name' => $data['name'],
                // 'partner_id' => intval($data['partner_id']),
                'partner_id' => $partner,

            ]);
            $qr_name=$department->name."_". Str::random(6).".png";
            $qr_code=QrCode::format('png')
            ->merge('/public/assets/img/logo.png', .3, false)
            ->size(512)->errorCorrection('H')->generate('D'.$department->id, public_path('storage/images/'.$qr_name));
            $department->update([
                'qr_code_department' =>  $qr_name,

            ]);
            if ($department) {
                event(new DepartmentCreated($department));
                return $department;
            }
            throw new GeneralException(trans('exceptions.backend.access.departments.create_error'));

        });
    }
    public function update(Department $department, array $data) : Department
    {
        if (strtolower($department->name) !== strtolower($data['name'])) {
            if ($this->departmentExists($data['name'])) {
                throw new GeneralException('A department already exists with the name '.$data['name']);
            }
        }
        return DB::transaction(function () use ($department, $data) {
         
            if ($department->update([
                'name' => $data['name'],
                // 'partner_id' => $data['partner_id'],
            ])) {
                return $department;
            }
            if ($department) {
                event(new DepartmentUpdated($department));
                return $department;
            }
            throw new GeneralException(trans('exceptions.backend.access.departments.update_error'));

        });
    }
    protected function departmentExists($name): bool
    {
        return $this->model
                ->where('name', strtolower($name))
                ->count() > 0;
    }
}
