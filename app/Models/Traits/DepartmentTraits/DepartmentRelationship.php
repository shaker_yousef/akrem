<?php


namespace App\Models\Traits\DepartmentTraits;


use App\Models\Profile;
use App\Models\Employee;


trait DepartmentRelationship
{
    public function profile(){
        return $this->belongsTo(Profile::class);
    }
    public function employees(){
        return $this->hasMany(Employee::class);

    }
}
