<?php


namespace App\Models\Traits\CountryTraits;


use App\Models\Profile;
use App\Models\User;
use App\Models\Customer;


trait CountryRelationship
{
    public function users(){
        return $this->hasMany(User::class);
    }
    public function profiles(){
        return $this->hasMany(Profile::class);
    }
    public function customers(){
        return $this->hasMany(Customer::class);
    }
}
