<?php

namespace App\Models\Traits\TipTraits;

use Illuminate\Support\Facades\DB;

trait TipMethod
{
    public function hasRole($tip){
        if ($this->tip()->where('name',$tip)->first()){
            return true;
        }
        return false;
    }
    
}
