<?php


namespace App\Models\Traits\TipTraits;


use App\Models\User;
use App\Models\Profile;

trait TipRelationship
{
  
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function profile(){
        return $this->belongsTo(Profile::class);
    }
    public function employee(){
        return $this->belongsTo(Employee::class);
    } 
   
}
