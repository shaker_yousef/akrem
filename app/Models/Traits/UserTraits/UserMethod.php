<?php

namespace App\Models\Traits\UserTraits;

/**
 * Trait UserMethod.
 */
trait UserMethod
{
    public function hasRole($role){
        if ($this->role()->where('name',$role)->first()){
            return true;
        }
        return false;
    }

    public function isAdmin()
    {
        return $this->hasRole(config('access.users.admin_role'));
    }
    public function isUser()
    {
        return $this->hasRole(config('access.users.default_role'));
    }
     public function isProvider()
    {
        return $this->hasRole('provider');
    }

    public function isEmployee()
    {
        return $this->hasRole('employee');
    }
    
   

    public static function defaultTip(){
        $default= DB::table('users')
            ->where('name',config('user'))->first();
        return $default->id;
    }
}
