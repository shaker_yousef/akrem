<?php

namespace App\Models\Traits\UserTraits;

use App\Models\Role;
use App\Models\Employee;
use App\Models\Profile;
use App\Models\Tip;
use App\Models\Country;
use App\Models\PaymentMethod;



/**
 * Class UserRelationship.
 */
trait UserRelationship
{
    public function role(){
        return $this->belongsTo(Role::class);
    }
    public function employees(){
        return $this->hasMany(Employee::class);
    }
    public function profiles(){
        return $this->hasMany(Profile::class);
    }
    public function tips(){
        return $this->hasMany(Tip::class);
    }
    public function country(){
        return $this->belongsTo(Country::class);
    }
    public function paymentmethods(){
        return $this->hasMany(PaymentMethod::class);
    }
}
