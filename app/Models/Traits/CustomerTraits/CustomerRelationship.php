<?php


namespace App\Models\Traits\CustomerTraits;


use App\Models\Profile;
use App\Models\User;
use App\Models\Country;


trait CustomerRelationship
{
    public function country(){
        return $this->belongsTo(Country::class);
    }
}
