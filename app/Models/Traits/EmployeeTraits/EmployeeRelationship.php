<?php

namespace App\Models\Traits\EmployeeTraits;

use App\Models\User;
use App\Models\Profile;
use App\Models\Department;
use App\Models\Bankaccount;




/**
 * Class UserRelationship.
 */
trait EmployeeRelationship
{
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function profile(){
        return $this->belongsTo(Profile::class,'provider_id','id');
    }
    public function department(){
        return $this->belongsTo(Department::class);
    }
    public function tips(){
        return $this->hasMany(Tip::class);
    }
}
