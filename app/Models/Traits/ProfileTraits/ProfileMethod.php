<?php

namespace App\Models\Traits\ProfileTraits;

use Illuminate\Support\Facades\DB;

trait ProfileMethod
{
    public function isActive()
    {
        return $this->status;
    }
    public function isConfirm()
    {
        return $this->confirm_status;
    }
    public function isDeactive()
    {
        return $this->status;
    }

    public static function getProvider($id)
    {
        $profile = Profile::where('id', $id)->first();
        return $profile_id = $profile->id;
    }
}
