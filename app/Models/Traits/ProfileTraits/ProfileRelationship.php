<?php

namespace App\Models\Traits\ProfileTraits;

use App\Models\Profile;
use App\Models\User;
use App\Models\Tip;
use App\Models\Employee;
use App\Models\Department;
use App\Models\Country;
use App\Models\Service;



trait ProfileRelationship
{
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function tips(){
        return $this->hasMany(Tip::class);
    }
    public function employees(){
        return $this->hasMany(Employee::class);
    }
    public function departments(){
        return $this->hasMany(Department::class);
    }
    public function country(){
        return $this->belongsTo(Country::class);
    }
    
    
}
