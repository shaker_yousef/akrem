<?php

namespace App\Models\Traits\RoleTraits;

use Illuminate\Support\Facades\DB;

trait RoleMethod
{
    public static function adminRole()
    {
        $admin= DB::table('roles')
            ->where('name',config('access.users.admin_role'))->first();
        return $admin->id;
    }

    public static function defaultRole(){
        $default= DB::table('roles')
            ->where('name',config('access.users.default_role'))->first();
        return $default->id;
    }
}
