<?php


namespace App\Models\Traits\PaymentMethodTraits;


use App\Models\User;
use App\Models\Profile;

trait PaymentMethodRelationship
{
  
    public function user(){
        return $this->belongsTo(User::class);
    }
   
}
