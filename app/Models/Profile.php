<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\ProfileTraits\ProfileMethod;
use App\Models\Traits\ProfileTraits\ProfileRelationship;
use App\Models\Traits\ProfileTraits\ProfileScope;

class Profile extends Model
{
    use HasFactory,ProfileRelationship,ProfileScope,ProfileMethod;
    public static $DEFAULT_STATUS=0;
    public static $DEFAULT_CONFIRMATION_STATUS=0;
    protected $fillable = [
        'partner_name',
        'bank_details',
        'bank_name',
        'trn',
        'transfer_customer_name',
        'ac',
        'iban',
        'swift_code',
        'company_registration_document',
        'phone_number',
        'mobile_number',
        'contact_email',
        'password',
        'website',
        'address',
        'logo',
        'services',
        'credit_method',
        'status',
        'confirmation_status',
        'qr_code_provider',
        'country_id',
        'fees',
        'is_fixed',
        'special_fees'
    ];
    /**
     * Set the categories
     *
     */
    public function setServicesAttribute($value)
    {
        $this->attributes['services'] = json_encode($value);
    }
     /**
     * Get the categories
     *
     */
    public function getServicesAttribute($value)
    {
        return $this->attributes['services'] = json_decode($value);
    }
}
