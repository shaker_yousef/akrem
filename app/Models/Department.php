<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\DepartmentTraits\DepartmentMethod;
use App\Models\Traits\DepartmentTraits\DepartmentRelationship;
use App\Models\Traits\DepartmentTraits\DepartmentScope;

class Department extends Model
{
    use HasFactory,DepartmentRelationship,DepartmentScope,DepartmentMethod;
     protected $fillable = [
        'name',
        'partner_id',
        'qr_code_department',
    ];
}
