<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\EmployeeTraits\EmployeeMethod;
use App\Models\Traits\EmployeeTraits\EmployeeRelationship;
use App\Models\Traits\EmployeeTraits\EmployeeScope;

class Employee extends Model
{
    use HasFactory,EmployeeRelationship,EmployeeScope,EmployeeMethod;
    protected $fillable = [
        'full_name',
        'work_title',
        'department_id',
        'id_number',
        'id_front_pic',
        'id_back_pic',
        'bank_details',
        'bank_name',
        'transfer_customer_name',
        'ac',
        'iban',
        'swift_code',
        'mobile_number',
        'email',
        'password',
        'credit_frequency',
        'profile_image',
        'provider_id',
        'qr_code',
        'is_active',
    ];
    
}
