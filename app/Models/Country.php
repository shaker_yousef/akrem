<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\CountryTraits\CountryMethod;
use App\Models\Traits\CountryTraits\CountryRelationship;
use App\Models\Traits\CountryTraits\CountryScope;

class Country extends Model
{
    use HasFactory,CountryRelationship,CountryScope,CountryMethod;
    protected $fillable = [
        'name',
        'flag',
        'currency',
        'fees',
    ];
}
