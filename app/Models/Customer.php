<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\CustomerTraits\CustomerMethod;
use App\Models\Traits\CustomerTraits\CustomerRelationship;
use App\Models\Traits\CustomerTraits\CustomerScope;

class Customer extends Model
{
    use HasFactory,CustomerRelationship,CustomerScope,CustomerMethod;
    protected $fillable = [
        'name',
        'email',
        'phone_number',
        'country_id',
        'is_active',
    ];
}
