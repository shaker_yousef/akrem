<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\PaymentMethodTraits\PaymentMethodRelationship;
use App\Models\Traits\PaymentMethodTraits\PaymentMethodScope;
use App\Models\Traits\PaymentMethodTraits\PaymentMethodMethod;

class PaymentMethod extends Model
{
    use HasFactory,PaymentMethodRelationship,PaymentMethodScope,PaymentMethodMethod;
    protected $fillable = [
        'user_id',
        'token',
        'card_type',
        'card_scheme',
        'payment_description',
        'tran_ref',
    ];
}
