<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\TipTraits\TipMethod;
use App\Models\Traits\TipTraits\TipRelationship;
use App\Models\Traits\TipTraits\TipScope;


class Tip extends Model
{
    use HasFactory,TipRelationship,TipScope,TipMethod;
    protected $fillable = [
        'amount',
        'payment_method',
        'transaction_id',
        'user_id',
        'partner_id',
        'department_id',
        'employee_id',
        'partner',
        'department',
        'employee',
        'receiver',
        'currency',
        'date_time',
    ];
    protected $casts = [
        'amount' => 'float',
    ];
}
